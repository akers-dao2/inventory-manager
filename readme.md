## Inventory Manager

---------
	
Customized real-time Inventory Management System.

A customized real-time Inventory Management application that mirrors the client’s inventory workflow to improve overall efficiency.  Handling functions like returns, repairs, historical product information, etc.

**Installation:**

You need to node and npm. At the root of the project type:

```node
npm install
```

**Update Firebase Configuration:**
```
src/environments/environment.ts
```

**Run Project:**

```node
npm run start
```

**Software Used:**

1. Angular (https://angular.io)
1. Angular Material (https://material.angular.io/)
1. Ionic (http://ionicframework.com)
1. FireBase (https://www.firebase.com)
1. Akita (https://github.com/datorama/akita)


----------
**Features:**

- Printing Reports
- Real-Time Data Storage
- Offline Compatible via Service Worker
- Akita State Management
- PWA (Installable, Background Updating & Push Notification)
- Responsive Design Layout

