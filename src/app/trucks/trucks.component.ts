import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { TrucksQuery } from '../shared/tables/state/trucks.query';
import { TrucksService } from '../shared/tables/state/trucks.service';
import { Truck } from '../shared/tables/state/truck.model';
import { Observable } from 'rxjs';
import { FormControl } from '@angular/forms';
import { distinctUntilChanged, tap } from 'rxjs/operators';

@Component({
  selector: 'app-trucks',
  templateUrl: './trucks.component.html',
  styleUrls: ['./trucks.component.scss']
})
export class TrucksComponent implements OnInit {
  public trucks: Observable<Truck[]>;
  public name = new FormControl('');

  constructor(
    private trucksQuery: TrucksQuery,
    private trucksService: TrucksService
  ) { }

  ngOnInit() {
    this.trucks = this.trucksQuery.selectAllSorted();
  }

  public upsert() {
    const truck = this.name.value;
    const result = typeof truck === 'string' ? { name: truck, id: undefined } : truck;
    this.trucksService.upsert(result);
    this.name.reset();
  }

  public remove(truck: Truck) {
    this.trucksService.remove(truck);
  }

}
