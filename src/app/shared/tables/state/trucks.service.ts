import { Injectable } from '@angular/core';
import { ID } from '@datorama/akita';
import { TrucksStore } from './trucks.store';
import { HttpClient } from '@angular/common/http';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { Truck } from './truck.model';
import { tap, take, share, skip } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class TrucksService {
  private trucksCollection: AngularFirestoreCollection<Truck>;

  constructor(
    private store: TrucksStore,
    private afs: AngularFirestore
  ) {
    this.trucksCollection = afs.collection('trucks');
  }

  /**
   * fetch
   *
   * @memberof TrucksService
   */
  public fetch() {
    const trucks$ = this.trucksCollection.valueChanges()
      .pipe(
        tap(trucks => this.store.set(trucks)),
        share()
      );

    trucks$
      .pipe(skip(1))
      .subscribe();

    return trucks$.pipe(take(1)).toPromise();
  }

  public setActive(id: string) {
    this.store.setActive(id);
  }

  public async upsert(truck: Truck) {
    const id = truck.id ? truck.id : this.afs.createId();
    const method = truck.id ? 'update' : 'set';
    try {
      await this.trucksCollection.doc(id)[method]({ ...truck, id });
      // this.store.upsert(id, truck);
    } catch (error) {
      console.error(error);
    }
  }

  public async remove(truck: Truck) {
    try {
      await this.trucksCollection.doc(truck.id).delete();
      // this.store.remove(truck.id);
    } catch (error) {
      console.error(error);
    }
  }

}
