import { Injectable } from '@angular/core';
import { QueryEntity, ID, filterNil } from '@datorama/akita';
import { CheckoutStore, CheckoutState } from './checkout.store';
import { CheckoutItem, ItemIdData } from './checkout.model';
import { map, switchMap, take } from 'rxjs/operators';
import { UsersQuery } from './users.query';
import { Observable, combineLatest } from 'rxjs';
import * as moment from 'moment';


@Injectable({ providedIn: 'root' })
export class CheckoutQuery extends QueryEntity<CheckoutState, CheckoutItem> {
  private readonly notEqualZero = map(v => v !== 0);

  constructor(
    protected store: CheckoutStore,
    private user: UsersQuery
  ) {
    super(store);
  }

  /**
   * hasItemInQueue
   *
   * @returns
   * @memberof CheckoutQuery
   */
  public hasItemInQueue$() {
    return this.user.selectActive()
      .pipe(
        filterNil,
        take(1),
        switchMap(user => {
          return this.selectCount(v => {
            return user && v.userId === user.id && !!v.items.length && v.items.some(i => !['checkin', 'remove'].includes(i.type));
          });
        }),
        this.notEqualZero
      );
  }

  /**
   * hasCheckedOutItems
   *
   * @param {ID} id
   * @returns
   * @memberof CheckoutQuery
   */
  public hasCheckedOutItems(id: ID) {
    const checkedOutItem = this.getEntity(id);
    return checkedOutItem.items.some(v => v.isDirty) || checkedOutItem.items.some(v => v.type !== 'checkout');
  }

  /**
   * getAllNotCheckInItems
   *
   * @returns {Observable<ItemIdData[]>}
   * @memberof CheckoutQuery
   */
  public getAllNotCheckInItems$(all = true): Promise<ItemIdData[]> {
    const date = new Date(moment().format('MMM DD YYYY')).valueOf();
    return this.user.selectActive()
      .pipe(
        filterNil,
        take(1),
        map(user => {
          return this.getAll({ filterBy: v => v.userId === user.id })
            .filter(v => all ? true : v.date === date)
            .reduce((acc, v) => acc.concat(v.items.map(r => ({ ...r, checkOutId: v.id }))), [])
            .filter(v => !['checkin', 'remove'].includes(v.type));
        }),
      )
      .toPromise();
  }

  /**
   * Checkoutitem that matches the active userid and current date
   *
   * @returns
   * @memberof CheckoutQuery
   */
  public getOpenCheckOutList() {
    const date = new Date(moment().format('MMM DD YYYY')).valueOf();
    return this.user.selectActive()
      .pipe(
        filterNil,
        take(1),
        switchMap(u => {
          return this.selectAll({ filterBy: c => c.userId === u.id && c.date === date });
        }),
        map(c => c[0]),
        take<CheckoutItem>(1)
      )
      .toPromise<CheckoutItem>();
  }
}
