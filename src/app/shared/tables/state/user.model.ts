import { ID, guid } from '@datorama/akita';

export interface User {
  id: string;
  userid: string;
  password: string;
  firstname: string;
  lastname: string;
  fullName?: string;
  authLevel: string;
}

export function createUser(params: Partial<User>) {
  return {
    ...params,
    fullName: params.firstname + ' ' + params.lastname
  } as User;
}
