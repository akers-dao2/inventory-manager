import * as moment from 'moment';

export type ItemType = 'new' | 'update' | 'remove' | 'checkout' | 'pending' | 'checkin' | 'checkout-pending' | 'untouched' | 'partial' | 'reject' | 'broken';

export interface ItemIdData {
  id: string;
  quantity: number;
  previousQuantity: number;
  isDirty: boolean;
  type: ItemType;
  checkOutId?: string;
  truckName?: string;
  truckId?: string;
  wasBroken?: boolean;
  status?: ItemType;
}

export interface CheckoutItem {
  id: string;
  items: ItemIdData[];
  userId: string;
  date: number;
}

export function createCheckout(params: Partial<CheckoutItem>) {
  return {
    ...params,
    date: new Date(moment().format('MMM DD YYYY')).valueOf(),
    items: []
  } as CheckoutItem;
}
