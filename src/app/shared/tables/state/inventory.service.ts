import { Injectable } from '@angular/core';
import { transaction } from '@datorama/akita';
import { InventoryStore } from './inventory.store';
import { InventoryItem } from './inventory.model';
import { ItemIdData, CheckoutItem, ItemType } from './checkout.model';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, take, switchAll, mergeMap } from 'rxjs/operators';
import { UsersQuery } from './users.query';
import * as papa from 'papaparse';
import { Subject, of } from 'rxjs';
import { InventoryQuery } from './inventory.query';
import { CheckoutQuery } from './checkout.query';
import { ToastService } from '../../services/toast.service';
import { SortInventoryService } from '../../services/sort-inventory.service';


@Injectable({ providedIn: 'root' })
export class InventoryService {

  private inventoryCollection: AngularFirestoreCollection<InventoryItem>;
  public removeItem$ = new Subject<string>();

  constructor(
    private store: InventoryStore,
    private query: InventoryQuery,
    private afs: AngularFirestore,
    private userQuery: UsersQuery,
    private checkoutQuery: CheckoutQuery,
    private toastService: ToastService,
    private sortService: SortInventoryService
  ) {
    this.inventoryCollection = afs.collection('inventory');
  }

  public async upsert(item: InventoryItem) {
    const id = item.id || this.afs.createId();
    item = { ...item, id };
    try {
      await this.inventoryCollection.doc(id).set(this.sanitize(item));
      this.store.upsert(id, item);
      this.store.setActive(id);
    } catch (error) {
      this.toastService.present(error.message, true);
    }
  }

  public async updateOrder(inventoryItems: InventoryItem[]) {
    const batch = this.afs.firestore.batch();

    try {
      inventoryItems.forEach((d, i) => {
        const item = { ...d, order: i };
        const docRef = this.afs.firestore.collection('inventory').doc(item.id);
        batch.set(docRef, item);
      });

      await batch.commit();
    } catch (error) {
      console.error(error);
    }
  }

  private sanitize(item: InventoryItem) {
    let rest: Partial<InventoryItem>,
      isChecked: boolean,
      status: string,
      quantity: number,
      isCheckedOut: boolean,
      isCheckedIn: boolean;
    ({ isChecked, isCheckedIn, isCheckedOut, status, quantity, ...rest } = item);

    return rest;
  }

  public resetCheckState(item: InventoryItem) {
    this.store.upsert(item.id, item);
  }

  public update(id: string | any, item: Partial<InventoryItem>) {
    this.store.update(id, item);

  }

  private async updateInventoryItems(data: InventoryItem, fullLoad = false) {
    // add new inventory item
    const hasInventory = this.query.hasEntity(data.id);

    if (!hasInventory) {
      this.store.upsert(data.id, {
        isChecked: false,
        status: 'untouched',
        isCheckedOut: false,
        isCheckedIn: false,
        quantity: 0,
        ...data
      });

      this.store.setActive(data.id);

      return;
    }

    // Following section of code updates existing inventory item
    const checkoutItem = this.checkoutQuery.getAll({ filterBy: c => c.userId === this.userQuery.getActive().id && !(c.items.every(i => i.type === 'checkin')) });
    const sortedCheckOutItem = await of(checkoutItem).pipe(this.sortService.date('desc'), take(1)).toPromise();
    let record: ItemIdData;

    // search for checkout record
    for (let index = 0; index < sortedCheckOutItem.length; index++) {
      const c = sortedCheckOutItem[index];
      const item = c.items.find(cc => cc.id === data.id && !['checkin'].includes(cc.type));
      if (item) {
        record = item;
        break;
      }
    }

    if (record) {
      // update inventory item with generic or existing checkout record
      this.store.upsert(record.id, {
        isChecked: !['checkin', 'remove'].includes(record.type),
        status: record.type,
        isCheckedOut: record.type === 'checkout' || record.type === 'checkout-pending',
        isCheckedIn: record.type === 'checkin',
        quantity: record.type === 'remove' ? 0 : record.quantity,
        ...data
      });
      this.store.setActive(record.id);
    } else if (!fullLoad) {
      this.store.update(data.id, { ...data });
      this.store.setActive(data.id);
    }

  }

  @transaction()
  fetch() {
    return new Promise(async (resolve) => {

      await this.afs.collection<InventoryItem>('inventory', c => c.orderBy('name'))
        .valueChanges()
        .pipe(
          take(1),
          tap(inventoryItems => {
            this.store.set(inventoryItems);
          })
        )
        .toPromise();

      // realtime sync of data
      this.afs.collection<InventoryItem>('inventory', c => c.orderBy('name'))
        .stateChanges()
        .pipe()
        .subscribe(inventoryItems => {
          inventoryItems
            .forEach(i => {
              // if (!i.payload.doc.metadata.hasPendingWrites) {
              if (i.type === 'removed') {
                this.store.remove(i.payload.doc.id);
                this.removeItem$.next(i.payload.doc.id);
              } else if (i.type === 'added') {
                this.updateInventoryItems(i.payload.doc.data(), true);
              } else {
                this.updateInventoryItems(i.payload.doc.data());
              }
              // }
            });

          if (resolve) {
            resolve();
          }
        });
    });
  }

  public loadCheckOutForUser() {
    const user = this.userQuery.getActive();
    try {
      this.afs.collection<CheckoutItem>('checkout', c => c.where('userId', '==', user.id))
        .valueChanges()
        .pipe(
          take(1),
          switchAll(),
          mergeMap(c => c.items),
          tap(c => {
            this.store.upsert(c.id, {
              isChecked: !['checkin', 'remove'].includes(c.type),
              status: c.type,
              isCheckedOut: c.type === 'checkout' || c.type === 'checkout-pending',
              isCheckedIn: c.type === 'checkin',
              quantity: c.quantity
            });
            this.store.setActive(c.id);
          })

        )
        .subscribe();
    } catch (error) {
      console.error(error);
    }
  }

  @transaction()
  public updateInitialQuantity(items: Partial<ItemIdData>[]) {
    items
      .forEach(item => {
        if (!['checkin', 'remove'].includes(item.type)) {
          this.store.update(item.id, () => {
            return {
              isChecked: true,
              quantity: item.quantity,
              status: item.type
            };
          });
        }
      });
  }

  public updateAvailAndQuantity(item: InventoryItem, quantity: number, existingQuantity: number): boolean {
    if (quantity === 0 || quantity === undefined) {
      this.store.update(item.id, { isChecked: existingQuantity !== 0 });
      return false;
    }

    if (quantity > item.available + existingQuantity) {
      this.store.update(item.id, { isChecked: existingQuantity !== 0 });
      this.store.setError({ msg: 'Error: Invalid quantity', item });
      return false;
    }

    return true;
  }

  public resetErrorState() {
    this.store.setError(null);
  }

  @transaction()
  public updateAvails(items: Partial<ItemIdData>[]) {
    let available: number;
    items.forEach(async item => {
      const entity = this.query.getEntity(item.id);
      switch (item.type) {
        case 'pending':
          available = entity.available - item.quantity;
          break;

        case 'update':
          available = entity.available + item.previousQuantity - item.quantity;
          break;

        case 'remove':
          available = entity.available + item.quantity;
          break;

        case 'reject':
          available = entity.available + item.quantity;
          break;
      }

      try {
        await this.inventoryCollection.doc(item.id).update({ available });
      } catch (error) {
        this.toastService.present(error.message, true);
      }

    });
  }

  public setActive(id: string) {
    this.store.setActive(id);
  }

  public updateState() {
    this.store.update(null, {});
  }

  public async remove(id: string) {
    try {
      await this.inventoryCollection.doc(id).delete();
    } catch (error) {
      this.toastService.present(error.message, true);
    }
  }

  public selectRemoveItem() {
    return this.removeItem$.asObservable();
  }

  public resetAllCheckStates() {
    this.store.update(null,
      {
        isChecked: false,
        quantity: 0,
        status: 'untouched',
        isCheckedOut: false,
        isCheckedIn: false
      });
  }

  @transaction()
  public async uncheckItem(id: any, markFixed = false) {
    let record = {
      isChecked: false,
      quantity: 0,
      status: 'untouched' as ItemType,
      isCheckedIn: false,
      isCheckedOut: false
    };
    if (markFixed) {
      record = { ...record, broken: false } as any;
    }
    this.store.update(id, record);
  }

  public updateInventoryStatus(id: string, status: ItemType) {
    this.store.update(id, { status });
  }

  public batchLoad() {
    const data = [];
    papa.parse('./assets/file.csv', {
      download: true,
      step: (r => {
        const item = {
          location: r.data[0][0],
          name: r.data[0][1],
          standard: parseInt(r.data[0][2], 10),
          available: parseInt(r.data[0][3], 10),
          consumable: r.data[0][4].toString().toLowerCase() === 'true',
          reorder: parseInt(r.data[0][5], 10),
          vendor: r.data[0][6],
          order: data.length
        };
        data.push(item);
        this.upsert(item);
      })
    });
  }

  public hasEntity(id: string) {
    return this.query.hasEntity(id);
  }

  public async markBroken(id: string) {
    try {
      await this.inventoryCollection.doc(id).update({ broken: true });
      this.store.update(id, { broken: true });
    } catch (error) {
      this.toastService.present(error.message, true);
    }
  }

  public async markFix(id: string) {
    try {
      await this.inventoryCollection.doc(id).update({ broken: false });
      this.store.update(id, { broken: false });
    } catch (error) {
      this.toastService.present(error.message, true);
    }
  }
}
