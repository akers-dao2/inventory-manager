import { Injectable } from '@angular/core';
import { UsersStore } from './users.store';
import { ID } from '@datorama/akita';
import { UsersQuery } from './users.query';
import { AngularFirestore, AngularFirestoreCollection } from '@angular/fire/firestore';
import { tap, take } from 'rxjs/operators';
import { User } from './user.model';
import { Observable } from 'rxjs';


@Injectable({ providedIn: 'root' })
export class UsersService {

  private usersCollection: AngularFirestoreCollection<User>;

  constructor(
    private store: UsersStore,
    private afs: AngularFirestore
  ) {
    this.usersCollection = afs.collection('users');
  }

  /**
   * fetch
   *
   * @memberof UsersService
   */
  public fetch() {
    this.usersCollection.valueChanges()
      .pipe(
        tap(users => this.store.set(users)),
        take(1)
      )
      .subscribe();
  }

  /**
   * loadUser
   *
   * @param {User} user
   * @memberof UsersService
   */
  public loadUser(user: User) {
    this.store.upsert(user.id, user);
    this.store.setActive(user.id);
  }

  /**
   * add
   *
   * @param {User} user
   * @memberof UsersService
   */
  public async add(user: User) {
    try {
      const id = this.afs.createId();
      user = { ...user, id };
      await this.usersCollection.doc(id).set(user);
      this.store.upsert(id, user);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * remove
   *
   * @param {string} id
   * @memberof UsersService
   */
  public async remove(id: string) {
    try {
      await this.usersCollection.doc(id).delete();
      this.store.remove(id);
    } catch (error) {
      console.error(error);
    }
  }

  /**
   * setActive
   *
   * @param {ID} id
   * @memberof UsersService
   */
  public setActive(id: ID) {
    this.store.setActive(id);
  }

  /**
   * upsert
   *
   * @param {User} user
   * @memberof UsersService
   */
  public async upsert(user: User) {
    const id = user.id || this.afs.createId();
    user = { ...user, id };
    try {
      await this.usersCollection.doc(id).set(user);
    } catch (error) {
      console.error(error);
    }
  }

  public signOut() {
    this.store.setActive(null);
  }

}
