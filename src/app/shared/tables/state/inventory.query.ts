import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { InventoryStore, InventoryState } from './inventory.store';
import { InventoryItem } from './inventory.model';
import { map } from 'rxjs/operators';


@Injectable({ providedIn: 'root' })
export class InventoryQuery extends QueryEntity<InventoryState, InventoryItem> {
  private readonly notEqualZero = map(v => v !== 0);
  public hasSelectItem$ = this.selectCount(v => v.isChecked && v.status === 'pending').pipe(this.notEqualZero);

  constructor(protected store: InventoryStore) {
    super(store);
  }

}
