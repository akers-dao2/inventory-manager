import { ItemType } from './checkout.model';

export interface InventoryItem {
  id?: string;
  location: string;
  name: string;
  standard: number;
  available: number;
  reorder: number;
  consumable: boolean;
  isChecked?: boolean;
  isCheckedOut?: boolean;
  isCheckedIn?: boolean;
  allowZeroQty?: boolean;
  allowRepairs?: boolean;
  partialCml?: boolean;
  quantity?: number;
  vendor: string;
  status?: ItemType;
  order?: number;
  reorderStatus?: 'GOOD' | 'REORDER';
  broken?: boolean;
  hideInventoryReport?: boolean;
}

export function createInventoryItem(item: Partial<InventoryItem>) {
  return { ...item } as InventoryItem;
}
