import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { CheckoutItem } from './checkout.model';

export interface CheckoutState extends EntityState<CheckoutItem> { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'checkout' })
export class CheckoutStore extends EntityStore<CheckoutState, CheckoutItem> {

  constructor() {
    super();
  }

}

