import { Injectable } from '@angular/core';
import { ID, transaction, Order } from '@datorama/akita';
import { CheckoutStore } from './checkout.store';
import { User } from './user.model';
import { CheckoutQuery } from './checkout.query';
import { createCheckout, ItemIdData, ItemType, CheckoutItem } from './checkout.model';
import { AngularFirestore, AngularFirestoreCollection, DocumentChangeAction } from '@angular/fire/firestore';
import { InventoryService } from './inventory.service';
import { UsersQuery } from './users.query';
import { InventoryQuery } from './inventory.query';
import * as moment from 'moment';
import { TrucksQuery } from './trucks.query';
import { ToastService } from '../../services/toast.service';
import { Truck } from './truck.model';
import { take, tap, switchAll, map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CheckoutService {
  private checkoutCollection: AngularFirestoreCollection<CheckoutItem>;

  constructor(
    private store: CheckoutStore,
    private query: CheckoutQuery,
    private afs: AngularFirestore,
    private inventoryService: InventoryService,
    private inventoryQuery: InventoryQuery,
    private userQuery: UsersQuery,
    private trucksQuery: TrucksQuery,
    private toastService: ToastService,
  ) {
    this.checkoutCollection = afs.collection('checkout');
  }

  public upsertItem(checkOutId: string, itemId: string, newState: any) {
    const checkOutItems = this.query.getEntity(checkOutId);
    let updatedState;
    for (let index = 0; index < checkOutItems.items.length; index++) {
      const item = checkOutItems.items[index];
      if (item.id === itemId) {
        updatedState = { ...item, ...newState };
        break;
      }
    }
    this.store.upsert(checkOutId, updatedState);
  }

  public async createOrReplace(item: ItemIdData, user: User, isProcessReq = false) {
    let checkoutItems = this.query.getActive() as CheckoutItem;
    const id = this.afs.createId();
    if (!isProcessReq) {
      // add truck information to item
      const truck = this.trucksQuery.getActive() as Truck || this.trucksQuery.getAllSorted()[0] as Truck;
      item = { ...item, truckName: truck.name, truckId: truck.id };
    }
    if (checkoutItems === undefined || checkoutItems.userId !== user.id) {
      checkoutItems = createCheckout({ items: [item], userId: user.id, id });
      this.store.setActive(id);
    }
    const items = checkoutItems.items.filter(v => v.id !== item.id || v.type === 'checkin');
    const record = {
      ...checkoutItems,
      items: [item, ...items],
      id: checkoutItems.id
    };
    this.store.upsert(checkoutItems.id, record);
  }

  @transaction()
  public markPristene(ids: string[]) {
    const checkoutItems = this.query.getActive() as CheckoutItem;
    ids.forEach(async id => {
      const itemIndex = checkoutItems.items.findIndex(v => v.id === id);
      const spliceCheckoutItems = checkoutItems.items.slice(0);
      const item = spliceCheckoutItems.splice(itemIndex, 1)[0];
      let updateItems = [];
      if (item.type === 'remove') {
        updateItems = [{ ...item, isDirty: false, type: 'remove' }];
      } else {
        updateItems = [{ ...item, isDirty: false, type: 'pending' }];
      }

      const record = {
        ...checkoutItems,
        items: spliceCheckoutItems.concat(updateItems)
      };

      this.store.update(checkoutItems.id, record);
      try {
        await this.checkoutCollection.doc(checkoutItems.id).set(record);
      } catch (error) {
        this.toastService.present(error.message, true);
      }

    });
  }


  public async remove(id: string) {
    try {
      const checkoutItems = this.query.getAll({ filterBy: c => c.userId === this.userQuery.getActive().id });
      checkoutItems.forEach(ck => {
        const items = ck.items.filter(c => c.id !== id);
        this.store.upsert(ck.id, {
          items
        });
      });
    } catch (error) {
      console.error(error);
      this.toastService.present(error.message, true);
    }
  }



  public async updateInventoryForCheckIn(id: string, itemId: ID, type: ItemType) {
    const checkoutItems = this.query.getEntity(id);
    const items = checkoutItems.items.slice(0);
    const index = items.findIndex(p => p.id === itemId);
    const itemToUpdate = { ...items.splice(index, 1)[0] };
    const inventoryItem = this.inventoryQuery.getEntity(itemToUpdate.id);
    if (!inventoryItem.consumable && type === 'checkin') {
      const available = itemToUpdate.quantity + inventoryItem.available;
      try {
        await this.afs.collection('inventory').doc(itemToUpdate.id).update({ available, broken: false });
        this.inventoryService.update(itemToUpdate.id, { available, broken: false });
      } catch (error) {
        this.toastService.present(error.message, true);
      }
    }
  }

  public async updateAllCheckOutType(id: string) {
    const checkoutItems = this.query.getEntity(id);

    try {
      await this.checkoutCollection.doc(id).set(checkoutItems);
      this.toastService.present('Successfully Updated');
    } catch (error) {
      this.toastService.present(error.message, true);
    }

  }

  public isPending(id: ID, itemId: ID) {
    const checkoutItems = this.query.getEntity(id);
    const checkoutItem = checkoutItems.items.find(v => v.id === itemId && v.type !== 'checkin');
    return checkoutItem ? ['pending', 'checkout-pending'].includes(checkoutItem.type) : true;
  }

  public isCheckedOut(id: ID, itemId: ID) {
    const checkoutItems = this.query.getEntity(id);
    const checkoutItem = checkoutItems.items.find(v => v.id === itemId && v.type !== 'checkin');
    return checkoutItem ? ['checkout', 'partial'].includes(checkoutItem.type) : false;
  }

  public isBroken(id: ID, itemId: ID) {
    const checkoutItems = this.query.getEntity(id);
    const checkoutItem = checkoutItems.items.find(v => v.id === itemId && v.type !== 'checkin');
    return checkoutItem ? ['broken'].includes(checkoutItem.type) : false;
  }

  public setActive(id: ID) {
    this.store.setActive(id);
  }

  public setActiveForActiveUser() {
    const user = this.userQuery.getActive();
    const date = new Date(moment().format('MMM DD YYYY')).valueOf();
    const checkItems = this.query.getAll({ filterBy: c => c.date === date && c.userId === user.id });
    const id = !!checkItems.length ? checkItems[0].id : null;
    this.store.setActive(id);
  }

  private addChangeStatus$() {
    return map((r: DocumentChangeAction<CheckoutItem>) => {
      const co = r.payload.doc.data() as CheckoutItem;
      if (r.payload.type === 'modified') {
        this.store.setActive(co.id);
      }

      return [co, r];
    });
  }

  @transaction()
  public fetch() {
    return new Promise(async (resolve) => {
      const user = this.userQuery.getActive();
      this.afs.collection<CheckoutItem>('checkout', c => c.orderBy('date') && c.where('userId', '==', user.id))
        .valueChanges()
        .pipe(
          take(1),
          tap(c => this.store.set(c))
        )
        .subscribe();

      this.afs.collection<CheckoutItem>('checkout', c => c.orderBy('date'))
        .stateChanges()
        .pipe(
          switchAll(),
          this.addChangeStatus$(),
          tap(result => {
            const cc = result[0] as CheckoutItem;
            const r = result[1] as DocumentChangeAction<CheckoutItem>;
            if (!r.payload.doc.metadata.hasPendingWrites) {
              this.store.upsert(cc.id, cc);
              if (r.payload.type === 'modified' && cc.userId === this.userQuery.getActive().id) {

                cc.items.forEach(c => {
                  if (this.inventoryService.hasEntity(c.id)) {
                    const existingItem = this.inventoryQuery.getEntity(c.id);
                    const statusChange = existingItem.status !== c.type;
                    const quantityChange = existingItem.quantity !== c.quantity && existingItem.isChecked;
                    if (statusChange || quantityChange) {

                      this.inventoryService.update(c.id, {
                        isChecked: !['checkin', 'remove'].includes(c.type),
                        status: c.type,
                        isCheckedOut: c.type === 'checkout' || c.type === 'checkout-pending',
                        isCheckedIn: c.type === 'checkin',
                        quantity: c.type === 'remove' ? 0 : c.quantity,
                      });

                      this.inventoryService.setActive(c.id);
                    }
                  }
                });
              }

            }
          })
        )
        .subscribe(() => {
          if (resolve) {
            resolve();
          }
        });

    });
  }

  public async updateUserQuantity(item: ItemIdData, quantity: number) {
    const checkoutItems = this.query.getActive() as CheckoutItem;
    const items = checkoutItems.items.filter(v => v.id !== item.id);
    const record = {
      ...checkoutItems,
      items: [{ ...item, previousQuantity: item.quantity, quantity }, ...items],
      id: checkoutItems.id
    };

    try {
      await this.checkoutCollection.doc(checkoutItems.id).set(record);
      this.store.upsert(checkoutItems.id, record);
    } catch (error) {
      this.toastService.present(error.message, true);
    }

  }
}
