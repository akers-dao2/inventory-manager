import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { Truck } from './truck.model';

export interface TrucksState extends EntityState<Truck> {}

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'trucks' })
export class TrucksStore extends EntityStore<TrucksState, Truck> {

  constructor() {
    super();
  }

}

