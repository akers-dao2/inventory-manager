import { Injectable } from '@angular/core';
import { QueryEntity } from '@datorama/akita';
import { TrucksStore, TrucksState } from './trucks.store';
import { Truck } from './truck.model';

@Injectable({ providedIn: 'root' })
export class TrucksQuery extends QueryEntity<TrucksState, Truck> {

  constructor(protected store: TrucksStore) {
    super(store);
  }

  getAllSorted() {
    return this.getAll({ sortBy: this.sort });
  }

  selectAllSorted() {
    return this.selectAll({ sortBy: this.sort });
  }

  private sort(a: any, b: any) {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;

  }

}
