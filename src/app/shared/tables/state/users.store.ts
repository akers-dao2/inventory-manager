import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig, ActiveState } from '@datorama/akita';
import { User } from './user.model';

export interface UsersState extends EntityState<User>, ActiveState { }

@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'users' })
export class UsersStore extends EntityStore<UsersState, User> {

  constructor() {
    super();
  }

}

