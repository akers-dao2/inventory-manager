import { Injectable } from '@angular/core';
import { EntityState, EntityStore, StoreConfig } from '@datorama/akita';
import { InventoryItem } from './inventory.model';

export interface InventoryState extends EntityState<InventoryItem> { }


@Injectable({ providedIn: 'root' })
@StoreConfig({ name: 'inventory' })
export class InventoryStore extends EntityStore<InventoryState, InventoryItem> {
  constructor() {
    super();
  }

}

