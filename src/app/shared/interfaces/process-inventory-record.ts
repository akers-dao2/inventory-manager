import { InventoryItem } from '../tables/state';

export interface InventoryRecord extends InventoryItem {
    truck: string;
    date: number;
}

export interface ProcessInventoryRecord {
    name: string;
    id: string;
    records: InventoryRecord[];
    allChecked: boolean;
    userId: string;
    date: number;
}