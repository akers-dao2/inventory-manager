export interface FilterPanelFormData {
    stateDate: string;
    endDate: string;
    name: string;
    user: string;
    truck: string;
    status: string;
}