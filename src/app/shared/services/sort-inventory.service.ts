import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SortInventoryService {

  constructor() { }

  public name = map((i: any[]) => i.sort((a, b) => {
    const nameA = a.name.toUpperCase();
    const nameB = b.name.toUpperCase();
    return this.sort(nameA, nameB);
  }));

  public date = (sortOrder = 'asc') => {
    return map((i: any[]) => i.sort((a, b) => {
      const dateA = a.date;
      const dateB = b.date;
      return sortOrder === 'asc' ? this.sort(dateA, dateB) : this.sort(dateB, dateA);
    }));
  }

  private sort(a, b) {
    if (a < b) {
      return -1;
    }
    if (a > b) {
      return 1;
    }
    return 0;
  }
}
