import { Injectable } from '@angular/core';
import { AlertController, LoadingController, PopoverController } from '@ionic/angular';
import { UsersQuery, UsersService, CheckoutQuery, CheckoutStore, InventoryService, User, CheckoutService, CheckoutItem } from '../tables/state';
import * as moment from 'moment';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { tap, map, take, switchAll, filter, mergeMap } from 'rxjs/operators';
import { filterNil } from '@datorama/akita';
import { combineLatest } from 'rxjs';
import { ToastService } from './toast.service';
import { PopoverLoginComponent } from '../components/popover-login/popover-login.component';
import { TrucksQuery } from '../tables/state/trucks.query';
import { TrucksService } from '../tables/state/trucks.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  constructor(
    private usersQuery: UsersQuery,
    private userService: UsersService,
    private checkoutQuery: CheckoutQuery,
    private checkoutStore: CheckoutStore,
    private checkoutService: CheckoutService,
    private inventoryService: InventoryService,
    public afAuth: AngularFireAuth,
    public afs: AngularFirestore,
    private toastService: ToastService,
    private popoverController: PopoverController,
    private trucksQuery: TrucksQuery,
    private trucksService: TrucksService,
    private route: Router
  ) { }

  /**
   * loginPrompt
   *
   * @private
   * @memberof LoginService
   */
  public async prompt() {
    const user = this.usersQuery.getActive();

    if (user === undefined) {
      this.displaySignInPopover();
    } else {
      const date = new Date(moment().format('MMM DD YYYY')).valueOf();
      const checkOutItems = this.checkoutQuery.getActive();

      try {
        await this.afAuth.auth.signInWithEmailAndPassword(user.userid, user.password);
      } catch (error) {
        console.error(error);
      }

      if (checkOutItems === undefined || date !== (checkOutItems as CheckoutItem).date) {
        this.checkoutStore.setActive(null);
        this.inventoryService.resetAllCheckStates();
      }

      await this.checkoutService.fetch();
      await this.inventoryService.fetch();
      this.userService.fetch();
      this.trucksService.fetch();
    }
  }

  public async onLogOut() {
    this.route.navigateByUrl('/inventory');
    this.checkoutStore.setActive(null);
    this.userService.signOut();
    await this.afAuth.auth.signOut();
    localStorage.removeItem('AkitaStores');
    window.location.reload();
  }

  public get isAdmin() {
    return combineLatest(
      this.usersQuery.selectAll().pipe(filter(v => !!v.length)),
      this.afAuth.authState.pipe(filterNil)
    )
      .pipe(
        filterNil,
        map(([c]) => c),
        take(1),
        switchAll(),
        mergeMap(() => this.afAuth.authState.pipe(filter(a => a !== null)), (u, a) => ({ ...u, currentUserEmail: a.email })),
        filter(u => u.userid === u.currentUserEmail),
        map(e => e.authLevel === 'admin')
      );
  }

  /**
   * On info click
   *
   * @param {InventoryItem} item
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  private async displaySignInPopover() {
    await this.trucksService.fetch();
    const trucks = this.trucksQuery.getAllSorted();
    const popover = await this.popoverController.create({
      component: PopoverLoginComponent,
      backdropDismiss: false,
      componentProps: { trucks },
      mode: 'md'
    });

    await popover.present();
    const result = await popover.onDidDismiss();
    this.handleLogin(result.data);
  }

  private async handleLogin(userInfo: { userid: string, password: string, truck: string }) {
    try {
      await this.afAuth.auth.signInWithEmailAndPassword(userInfo.userid, userInfo.password);
      this.afs.collection<User>('users', ref =>
        ref.where('userid', '==', userInfo.userid)
      ).valueChanges()
        .pipe(
          tap(async v => {
            if (!v.length) {
              this.toastService.present('Error user not found in USERS table.');
              this.prompt();
              return;
            }
            const user = v[0];
            this.userService.loadUser(user);
            this.userService.fetch();
            await this.checkoutService.fetch();
            await this.inventoryService.fetch();
            this.trucksService.setActive(userInfo.truck);
          }),
          take(1)
        )
        .subscribe();


    } catch (error) {
      this.toastService.present(error.message);
      this.prompt();
      console.error(error);
    }
  }
}
