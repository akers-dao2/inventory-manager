import { Injectable } from '@angular/core';
import { ToastController } from '@ionic/angular';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) { }

  /**
   * Present toast message
   *
   * @private
   * @param {string} message
   * @memberof AddComponent
   */
  public async present(message: string, error = false) {
    const toast = await this.toastController.create({
      message,
      duration: error ? 0 : 2000,
      showCloseButton: error ? true : false
    });
    toast.present();
  }
}
