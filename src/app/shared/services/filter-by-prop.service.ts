import { Injectable } from '@angular/core';
import { DatePipe } from '@angular/common';
import { PopoverController, ToastController } from '@ionic/angular';
import { PopoverFilterComponent } from '../components/popover-filter/popover-filter.component';
import { ProcessInventoryRecord } from '../interfaces/process-inventory-record';

@Injectable({
  providedIn: 'root'
})
export class FilterByPropService {
  private toast: HTMLIonToastElement;
  private datePipe = new DatePipe('en-US');
  constructor(
    private popoverController: PopoverController,
    private toastController: ToastController,
  ) { }

  /**
   *
   *
   * @param {*} $event
   * @param {string} prop
   * @param {ProcessInventoryRecord[]} origReportsData
   * @param {ProcessInventoryRecord[]} reportsData
   * @param {Map<string, string>} cache
   * @memberof FilterByPropService
   */
  public async execute($event, prop: string, origReportsData: ProcessInventoryRecord[], reportsData: ProcessInventoryRecord[], cache: Map<string, string>) {
    let inventoryItems = origReportsData.map(r => r.records)
      .reduce((acc, r) => [...acc, ...r], [])
      .map(r => ({ name: r[prop], value: r[prop] }))
      .filter(r => r.name !== undefined)
      .reduce((acc: { name: string, value: string }[], r: { name: string, value: string }) => {
        return acc.some(a => a.name === r.name) ? acc : acc.concat(r);
      }, [])
      .sort(this.sort.bind(this, 'name'));

    if (prop === 'date') {
      inventoryItems = this.convertDateToReadable(inventoryItems);
    }

    inventoryItems.unshift({ name: 'All', value: 'all' });
    const result = await this.filterPopover($event, inventoryItems);
    return this.filterList(result, prop, origReportsData, reportsData, cache);
  }

  /**
   *
   *
   * @private
   * @param {*} inventoryItems
   * @returns
   * @memberof FilterByPropService
   */
  private convertDateToReadable(inventoryItems) {
    return inventoryItems.map(i => {
      return {
        ...i,
        name: this.datePipe.transform(i.name)
      };
    });
  }

  /**
   *
   *
   * @private
   * @param {{ data: string }} result
   * @param {string} type
   * @param {ProcessInventoryRecord[]} origReportsData
   * @param {ProcessInventoryRecord[]} reportsData
   * @param {Map<string, string>} cache
   * @returns
   * @memberof FilterByPropService
   */
  private filterList(result: { data: string }, type: string, origReportsData: ProcessInventoryRecord[], reportsData: ProcessInventoryRecord[], cache: Map<string, string>) {
    if (result.data === undefined) {
      return;
    }
    if (cache.has(type)) {
      reportsData = this.getResetFilterForType(type, origReportsData, cache);
      if (result.data === 'all') {
        this.displayFilterByMessage(cache);
        return;
      }
    }

    switch (type) {
      case 'name':
      case 'truck':
      case 'status':
      case 'date':
        reportsData = this.getFilterList(result, type, reportsData);
        break;

      default:
        reportsData = reportsData.filter(d => d.name === result.data);
        break;
    }
    cache.set(type, result.data);
    this.displayFilterByMessage(cache);
    console.log(result);
    return reportsData;
  }

  /**
   *
   *
   * @private
   * @param {Map<string, string>} cache
   * @memberof FilterByPropService
   */
  private displayFilterByMessage(cache: Map<string, string>) {
    let message: string;
    cache.forEach((v, k) => {
      if (k === 'date') {
        v = this.datePipe.transform(v);
      }
      message = `${message ? message + ' & ' : ''} ${k.toUpperCase()}: ${v.toUpperCase()}`;
    });
    this.presentToast('Filtered By: ' + message);
  }

  private sort(prop: string, a: {}, b: {}) {
    const nameA = a[prop];
    const nameB = b[prop];
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;

  }

  /**
   *
   *
   * @private
   * @param {Event} ev
   * @param {any[]} names
   * @returns
   * @memberof FilterByPropService
   */
  private async filterPopover(ev: Event, names: any[]) {
    const popover = await this.popoverController.create({
      component: PopoverFilterComponent,
      event: ev,
      componentProps: { names }
    });

    await popover.present();

    return await popover.onDidDismiss() as { data: string };
  }

  /**
   *
   *
   * @private
   * @param {{ data: string }} result
   * @param {string} prop
   * @param {ProcessInventoryRecord[]} reportsData
   * @returns
   * @memberof FilterByPropService
   */
  private getFilterList(result: { data: string }, prop: string, reportsData: ProcessInventoryRecord[]) {
    return reportsData
      .filter(d => d.records.some(r => r[prop] === result.data))
      .reduce((acc, d) => {
        d = { ...d, records: d.records.filter(r => r[prop] === result.data) };
        return [...acc, d];
      }, []);
  }

  /**
   *
   *
   * @private
   * @param {string} message
   * @returns
   * @memberof FilterByPropService
   */
  private async presentToast(message: string) {
    if (!this.toast) {
      this.toast = await this.toastController.create({
        message,
        showCloseButton: true,
        closeButtonText: 'Hide'
      });

      this.toast.present();
    } else {
      if (message === 'Filtered By: undefined') {
        this.toast.dismiss();
        return;
      }
      this.toast.message = message;
    }

    this.toast.onDidDismiss()
      .then(() => {
        this.toast = undefined;
      });
  }

  private getResetFilterForType(type: string, origReportsData: ProcessInventoryRecord[], cache: Map<string, string>) {
    cache.delete(type);
    let resetReportsData = origReportsData.slice(0);

    cache.forEach((v, k) => {
      if (k === 'user') {
        resetReportsData = resetReportsData
          .filter(r => r.name === v);
        return;
      }
      resetReportsData = resetReportsData
        .filter(d => d.records.some(r => r[k] === v))
        .reduce((acc, d) => {
          d = { ...d, records: d.records.filter(r => r[k] === v) };
          return [...acc, d];
        }, []);
    });

    return resetReportsData;
  }
}
