import { TestBed } from '@angular/core/testing';

import { FilterByPropService } from './filter-by-prop.service';

describe('FilterByPropService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FilterByPropService = TestBed.get(FilterByPropService);
    expect(service).toBeTruthy();
  });
});
