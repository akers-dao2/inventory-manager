import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
    providedIn: 'root'
})
export class PrintService {
    public onPrint = new Subject<string>();
    constructor() { }

    printPage(name: string) {
        this.onPrint.next(name);
    }
}
