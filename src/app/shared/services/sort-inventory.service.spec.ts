import { TestBed } from '@angular/core/testing';

import { SortInventoryService } from './sort-inventory.service';

describe('SortInventoryService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SortInventoryService = TestBed.get(SortInventoryService);
    expect(service).toBeTruthy();
  });
});
