import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-filter',
  templateUrl: './popover-filter.component.html',
  styleUrls: ['./popover-filter.component.scss']
})
export class PopoverFilterComponent {

  public names: {name: string}[];

  constructor(private viewController: PopoverController) {
    this.viewController.getTop().then(v => {
      this.names = (v.componentProps as string[])['names'];
    });
  }

  public itemClick(item: string) {
    this.viewController.dismiss(item);
  }

}
