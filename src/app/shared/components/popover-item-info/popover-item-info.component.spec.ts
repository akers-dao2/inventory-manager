import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopoverItemInfoComponent } from './popover-item-info.component';

describe('PopoverItemInfoComponent', () => {
  let component: PopoverItemInfoComponent;
  let fixture: ComponentFixture<PopoverItemInfoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverItemInfoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopoverItemInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
