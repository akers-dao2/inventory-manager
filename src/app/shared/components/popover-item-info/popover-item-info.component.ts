import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';
import { InventoryItem } from '../../tables/state';

@Component({
  selector: 'app-popover-item-info',
  templateUrl: './popover-item-info.component.html',
  styleUrls: ['./popover-item-info.component.scss']
})
export class PopoverItemInfoComponent implements OnInit {
  public item: InventoryItem;

  constructor(private viewController: PopoverController) { }

  ngOnInit() {
    this.viewController.getTop().then(v => {
      this.item = (v.componentProps as InventoryItem)['item'];
    });
  }

}
