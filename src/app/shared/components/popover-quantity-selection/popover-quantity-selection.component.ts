import { Component, OnInit } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-quantity-selection',
  templateUrl: './popover-quantity-selection.component.html',
  styleUrls: ['./popover-quantity-selection.component.scss']
})
export class PopoverQuantitySelectionComponent implements OnInit {
  public title: string;
  public quantity: number;
  public hasCancelBtn = true;

  constructor(private viewController: PopoverController) {
    this.viewController.getTop().then(v => {
      this.title = (v.componentProps as string)['title'];
      this.quantity = (v.componentProps as string)['quantity'];
      if (this.quantity === undefined || this.quantity === 0) {
        this.quantity = 0;
        return;
      }
      this.hasCancelBtn = false;
    });
  }

  ngOnInit() {
  }

  public submit() {
    this.viewController.dismiss(this.quantity);
  }

  public cancel() {
    this.viewController.dismiss();
  }

  public onClick(type: string) {
    switch (type) {
      case 'add':
        this.quantity = ++this.quantity;
        return;
      case 'remove':
        this.quantity = !this.quantity ? 0 : --this.quantity;
        return;
    }
  }
}
