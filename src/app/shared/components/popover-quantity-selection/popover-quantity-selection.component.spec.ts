import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PopoverQuantitySelectionComponent } from './popover-quantity-selection.component';

describe('PopoverQuantitySelectionComponent', () => {
  let component: PopoverQuantitySelectionComponent;
  let fixture: ComponentFixture<PopoverQuantitySelectionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PopoverQuantitySelectionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PopoverQuantitySelectionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
