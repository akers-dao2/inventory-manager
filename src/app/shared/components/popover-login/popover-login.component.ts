import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PopoverController } from '@ionic/angular';
import { Truck } from '../../tables/state/truck.model';

@Component({
  selector: 'app-popover-login',
  templateUrl: './popover-login.component.html',
  styleUrls: ['./popover-login.component.scss']
})
export class PopoverLoginComponent implements OnInit {
  public signInForm: FormGroup;
  public trucks: Truck[];
  constructor(
    private fb: FormBuilder,
    private viewController: PopoverController,
  ) { }

  ngOnInit() {
    this.viewController.getTop().then(v => {
      this.trucks = (v.componentProps as Truck[])['trucks'];
    });
    this.createForm();
  }

  public signIn() {
    const userInfo = this.signInForm.value;
    this.viewController.dismiss(userInfo);
  }

  /**
   * Create a new form
   *
   * @private
   * @memberof AddComponent
   */
  private createForm() {
    this.signInForm = this.fb.group({
      userid: ['', Validators.required],
      password: ['', Validators.required],
      truck: ['', Validators.required],
    });
  }
}
