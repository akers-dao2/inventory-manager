import { Component } from '@angular/core';
import { PopoverController } from '@ionic/angular';

@Component({
  selector: 'app-popover-menu',
  templateUrl: './popover-menu.component.html',
  styleUrls: ['./popover-menu.component.scss']
})
export class PopoverMenuComponent {

  public list: { name: string, value: string };

  constructor(private viewController: PopoverController) {
    this.viewController.getTop().then(v => {
      this.list = (v.componentProps as { name: string, value: string })['list'].filter(r => r.show);
    });
  }

  public itemClick(item: string) {
    this.viewController.dismiss({ item });
  }

}
