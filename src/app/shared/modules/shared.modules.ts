import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { IonicModule } from '@ionic/angular';
import { AddOrUpdateComponent } from '../../add-or-update/add-or-update.component';
import { CheckboxColorPipe } from '../pipes/checkbox-color.pipe';
import { AddOrUpdateUserComponent } from '../../users/add-or-update-user/add-or-update-user.component';
import { ListUsersComponent } from '../../users/list-users/list-users.component';
import { DismissLoaderPipe } from '../pipes/dismiss-loader.pipe';


@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
  ],
  exports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    IonicModule,
    AddOrUpdateComponent,
    AddOrUpdateUserComponent,
    ListUsersComponent,
    CheckboxColorPipe,
    DismissLoaderPipe
  ],
  declarations: [
    AddOrUpdateComponent,
    AddOrUpdateUserComponent,
    ListUsersComponent,
    CheckboxColorPipe,
    DismissLoaderPipe
  ],
  entryComponents: [
    AddOrUpdateComponent,
    AddOrUpdateUserComponent
  ]
})
export class SharedModules { }
