import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'dismissLoader'
})
export class DismissLoaderPipe implements PipeTransform {

  transform(value: string, isLast: boolean, loader: HTMLIonLoadingElement): any {
    if (isLast) {
      loader.dismiss();
    }

    return value;
  }

}
