import { Pipe, PipeTransform } from '@angular/core';
import { InventoryItem } from '../tables/state';

@Pipe({
  name: 'checkboxColor'
})
export class CheckboxColorPipe implements PipeTransform {

  transform(value: InventoryItem) {
    return this.getColor(value);
  }

  private getColor(value: InventoryItem) {
    const color = new Map();
    color.set('checkout', 'medium');
    color.set('pending', 'primary');
    color.set('broken', 'medium');
    const status = value.broken ? 'broken' : value.status;
    return color.get(status);
  }

}
