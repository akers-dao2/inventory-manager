import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouteReuseStrategy } from '@angular/router';

import { IonicModule, IonicRouteStrategy } from '@ionic/angular';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { StatusBar } from '@ionic-native/status-bar/ngx';

import { AppComponent } from './app.component';
import { AppRoutingModule } from './app-routing.module';
import { PopoverMenuComponent } from './shared/components/popover-menu/popover-menu.component';
import { PopoverQuantitySelectionComponent } from './shared/components/popover-quantity-selection/popover-quantity-selection.component';
import { PopoverItemInfoComponent } from './shared/components/popover-item-info/popover-item-info.component';
import { SharedModules } from './shared/modules/shared.modules';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { AngularFireModule } from '@angular/fire';
import { AngularFireAuthModule } from '@angular/fire/auth';
import { AngularFirestoreModule } from '@angular/fire/firestore';
import { HttpClientModule } from '@angular/common/http';
import { PopoverFilterComponent } from './shared/components/popover-filter/popover-filter.component';
import { PopoverLoginComponent } from './shared/components/popover-login/popover-login.component';

@NgModule({
  declarations: [
    AppComponent,
    PopoverMenuComponent,
    PopoverQuantitySelectionComponent,
    PopoverItemInfoComponent,
    PopoverFilterComponent,
    PopoverLoginComponent
  ],
  entryComponents: [
    PopoverMenuComponent,
    PopoverQuantitySelectionComponent,
    PopoverItemInfoComponent,
    PopoverFilterComponent,
    PopoverLoginComponent
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    AppRoutingModule,
    SharedModules,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFirestoreModule.enablePersistence(),
    AngularFireAuthModule,
    AngularFirestoreModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    { provide: RouteReuseStrategy, useClass: IonicRouteStrategy }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
