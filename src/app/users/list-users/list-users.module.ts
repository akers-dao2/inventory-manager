import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModules } from '../../shared/modules/shared.modules';
import { ListUsersComponent } from './list-users.component';


@NgModule({
  imports: [
    SharedModules,
    RouterModule.forChild([
      {
        path: '',
        component: ListUsersComponent
      }
    ])
  ]
})
export class ListUsersModule {}
