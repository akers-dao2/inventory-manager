import { Component, OnInit } from '@angular/core';
import { AngularFirestoreCollection, AngularFirestore } from '@angular/fire/firestore';
import { CheckoutItem, User, UsersQuery } from '../../shared/tables/state';
import { Observable } from 'rxjs';
import { AddOrUpdateUserComponent } from '../add-or-update-user/add-or-update-user.component';
import { ModalController } from '@ionic/angular';
import { tap, take, filter } from 'rxjs/operators';
import { AngularFireAuth } from '@angular/fire/auth';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.scss']
})
export class ListUsersComponent implements OnInit {
  public list: Observable<User[]>;
  private userCollection: AngularFirestoreCollection<User>;


  constructor(
    private afs: AngularFirestore,
    public modalController: ModalController,
    private userQuery: UsersQuery,
    public afAuth: AngularFireAuth
  ) {
    this.userCollection = afs.collection('users');
  }

  ngOnInit() {
    this.afAuth.authState
      .pipe(
        filter(a => a !== null),
        take(1)
      )
      .subscribe(async d => {
        this.list = this.userCollection.valueChanges();
      });
  }

  public async onUserClick(id: string) {
    this.userCollection.doc<User>(id).valueChanges()
      .pipe(
        tap(async entity => {
          const modal = await this.modalController.create({
            component: AddOrUpdateUserComponent,
            componentProps: { entity }
          });
          await modal.present();
        }),
        take(1)
      )
      .subscribe();
  }

  public async onAddClick() {
    const modal = await this.modalController.create({
      component: AddOrUpdateUserComponent
    });
    await modal.present();
  }


}
