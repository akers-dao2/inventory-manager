import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { ToastController, ModalController } from '@ionic/angular';
import { UsersService, createUser, User, UsersQuery, CheckoutQuery, CheckoutService } from '../../shared/tables/state';
import { AngularFireAuth } from '@angular/fire/auth';


@Component({
  selector: 'app-add-user',
  templateUrl: './add-or-update-user.component.html',
  styleUrls: ['./add-or-update-user.component.scss']
})
export class AddOrUpdateUserComponent implements OnInit {
  public addUserForm: FormGroup;
  public isEdit = false;
  public authLevel = 'user';
  @Input() entity: User;

  constructor(
    private fb: FormBuilder,
    private usersService: UsersService,
    private usersQuery: UsersQuery,
    private checkoutQuery: CheckoutQuery,
    private checkoutService: CheckoutService,
    private toastController: ToastController,
    private modalController: ModalController,
    private afAuth: AngularFireAuth
  ) { }

  ngOnInit() {
    this.createForm();

    if (this.entity) {
      this.isEdit = true;
      let rest: Partial<User>,
        fullName: string,
        authLevel: string;
      ({ fullName, authLevel, ...rest } = this.entity);
      this.authLevel = this.entity.authLevel;
      this.addUserForm.setValue(rest);
    }
  }

  /**
   * Update or Insert user
   *
   * @memberof AddOrUpdateUserComponent
   */
  public async upsertItem() {
    const values = this.addUserForm.value;
    this.usersService.upsert(createUser({ ...values, authLevel: this.authLevel }));
    this.modalController.dismiss();

    if (!this.isEdit) {
      try {
        await this.afAuth.auth.createUserWithEmailAndPassword(values.userid, values.password);
        this.addUserForm.reset();
        this.presentToast('User Added Successfully');
      } catch (error) {
        console.error(error);
      }
    }
  }

  /**
   * removeUser
   *
   * @returns
   * @memberof AddOrUpdateUserComponent
   */
  public removeUser() {
    const user = this.usersQuery.getActive();
    if (user.id === this.entity.id) {
      this.presentToast('Error Deleting User: User Login');
      return;
    }
    this.usersService.remove(this.entity.id);
    this.checkoutQuery.getAll({ filterBy: c => c.userId === user.id })
      .forEach(async c => this.checkoutService.remove(c.id));
    this.presentToast('User Deleted Successfully');
    this.modalController.dismiss();
  }

  /**
   * onClose
   *
   * @memberof AddOrUpdateUserComponent
   */
  public onClose() {
    this.modalController.dismiss();
  }

  /**
   * onAuthChange
   *
   * @param {Event} $event
   * @memberof AddOrUpdateUserComponent
   */
  public onAuthChange({ detail }: { detail: { value: string } }) {
    this.authLevel = detail.value;
  }

  /**
  * Create a new form
  *
  * @private
  * @memberof AddComponent
  */
  private createForm() {
    this.addUserForm = this.fb.group({
      id: [''],
      firstname: ['', Validators.required],
      lastname: ['', Validators.required],
      userid: [, Validators.required],
      password: [, Validators.required]
    });
  }

  /**
  * Present toast message
  *
  * @private
  * @param {string} message
  * @memberof AddComponent
  */
  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

}
