import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { SharedModules } from '../../shared/modules/shared.modules';
import { AddOrUpdateUserComponent } from './add-or-update-user.component';

@NgModule({
  imports: [
    SharedModules,
    RouterModule.forChild([
      {
        path: '',
        component: AddOrUpdateUserComponent
      }
    ])
  ]
})
export class AddOrUpdateUserModule {}
