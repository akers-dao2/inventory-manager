import { Component } from '@angular/core';
import { LoginService } from './shared/services/login.service';
import { SwUpdate } from '@angular/service-worker';
import { ToastController } from '@ionic/angular';
import { tap } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  public appPages = [
    {
      title: 'Inventory',
      url: '/inventory',
      icon: 'briefcase'
    },
    {
      title: 'Process Request',
      url: '/process-inventory',
      icon: 'md-barcode'
    },
    {
      title: 'Add Inventory',
      url: '/add',
      icon: 'add'
    },
    {
      title: 'Reports',
      url: '/reports',
      icon: 'folder'
    },
    {
      title: 'List Users',
      url: '/list-users',
      icon: 'contacts'
    },
    {
      title: 'Trucks',
      url: '/trucks',
      icon: 'bus'
    },
    // {
    //   title: 'Settings',
    //   url: '/settings',
    //   icon: 'cog'
    // },
  ];

  public isAdmin: boolean;

  constructor(
    private loginService: LoginService,
    updates: SwUpdate,
    private toastController: ToastController
  ) {
    this.loginService.prompt();
    updates.available.subscribe(async event => {
      if (event) {
        await this.presentToastWithOptions();
        updates.activateUpdate()
          .then(() => {
            location.reload();
          });
      }
    });

    this.loginService.isAdmin
      .pipe(
        tap(isAdmin => {
          this.isAdmin = isAdmin;
        })
      )
      .subscribe();
  }

  async presentToastWithOptions() {
    const toast = await this.toastController.create({
      message: 'Update Application',
      showCloseButton: true,
      closeButtonText: 'OK'
    });

    toast.present();

    return toast.onDidDismiss();
  }

  public onLogOut() {
    this.loginService.onLogOut();
  }
}
