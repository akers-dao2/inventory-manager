import { Component, OnInit } from '@angular/core';
import {
  CheckoutQuery,
  InventoryQuery,
  UsersQuery,
  CheckoutItem,
  InventoryItem,
  InventoryService,
  CheckoutService,
  ItemIdData
} from '../shared/tables/state';
import { combineLatest, take, switchAll, filter, map, toArray, tap, distinctUntilChanged, reduce, auditTime } from 'rxjs/operators';
import { filterNil } from '@datorama/akita';
import { of, Observable } from 'rxjs';
import { ProcessInventoryRecord, InventoryRecord } from '../shared/interfaces/process-inventory-record';
import { PopoverController, ToastController } from '@ionic/angular';
import { PopoverMenuComponent } from '../shared/components/popover-menu/popover-menu.component';
import { PopoverQuantitySelectionComponent } from '../shared/components/popover-quantity-selection/popover-quantity-selection.component';


@Component({
  selector: 'app-process-inventory-request',
  templateUrl: './process-inventory-request.component.html',
  styleUrls: ['./process-inventory-request.component.scss']
})
export class ProcessInventoryRequestComponent implements OnInit {

  public view = 'check-out';
  public checkOutList: ProcessInventoryRecord[] = [];
  public originalCheckOutList: ProcessInventoryRecord[] = [];
  public originalCheckInList: ProcessInventoryRecord[] = [];
  public originalBrokenList: ProcessInventoryRecord[] = [];
  public isEditMode = false;
  public searchInput = '';
  public displayCheckOutBtn = false;
  public displayCheckInBtn = false;

  constructor(
    private checkoutQuery: CheckoutQuery,
    private checkoutService: CheckoutService,
    private inventoryQuery: InventoryQuery,
    private inventoryService: InventoryService,
    private userQuery: UsersQuery,
    private popoverController: PopoverController,
    private toastController: ToastController,
  ) { }

  ngOnInit() {

    this.checkoutQuery.selectAll()
      .pipe(
        combineLatest((this.inventoryQuery.selectAll() as Observable<InventoryItem[]>).pipe(distinctUntilChanged(), filter(i => !!i.length))),
        auditTime(0),
        filterNil,
        // take(1),
        tap(() => this.getCheckOutItems())
      )
      .subscribe();

    // (this.checkoutQuery.selectActive() as Observable<CheckoutItem>)
    //   .pipe(
    //     filterNil,
    //     auditTime(0),
    //     this.updateActiveItem()
    //   )
    //   .subscribe(console.log);

    this.displayCheckOutBtn = this.hasCheckedOutItem;
  }

  private getChangeRecord(co: CheckoutItem) {
    const record = this.checkOutList.find(c => c.id === co.id && c.date === co.date);
    if (record) {
      for (let index = 0; index < record.records.length; index++) {
        const inventoryRecord = record.records[index];
        let coById = co.items.find(cc => cc.id === inventoryRecord.id && cc.type !== 'checkin');
        coById = { ...coById, status: coById.type };
        const propsToCompare = ['quantity', 'status'];
        for (let idx = 0; idx < propsToCompare.length; idx++) {
          const prop = propsToCompare[idx];
          const n = coById[prop];
          const o = inventoryRecord[prop];
          if (n !== o) {
            return coById;
          }
        }
      }
    }
    return undefined;
  }

  private updateActiveItem() {
    return tap<CheckoutItem>(v => {
      const changeRecord = this.getChangeRecord(v);
      const hasPending = v.items.some(i => i.type === 'pending');
      const checkoutListRecord = this.checkOutList.find(c => c.id === v.id);

      if (changeRecord !== undefined) {
        for (let index = 0; index < this.checkOutList.length; index++) {
          const item = this.checkOutList[index];
          if (v.id === item.id) {
            for (let idx = 0; idx < item.records.length; idx++) {
              const record = item.records[idx];
              if (record.id === changeRecord.id) {
                const inventoryItem = this.inventoryQuery.getEntity(record.id);
                // remove record
                if (changeRecord.type === 'remove') {
                  item.records = item.records.filter((_, i) => i !== idx);
                  if (!item.records.length) {
                    this.checkOutList = this.checkOutList.filter(c => c.id !== item.id);
                  }
                  return;
                }
                // update record
                item.records[idx] = { ...record, ...inventoryItem, quantity: changeRecord.quantity, status: changeRecord.type };
                return;
              }
            }
            // add new record to existing record
            // const newRecord = this.getProcessRequestRecord(changeRecord, v.date);
            // item.records = item.records.concat(newRecord);
            break;
          }
        }
      } else if (checkoutListRecord) {
        // add new record to existing record
        const origCheckoutListRecord = this.originalCheckOutList.find(c => c.id === v.id);
        const cRecord = v.items.find(c => !checkoutListRecord.records.some(r => r.id === c.id || ['checkin', 'remove'].includes(c.type)));
        const newRecord = this.getProcessRequestRecord(cRecord, v.date);
        checkoutListRecord.records = checkoutListRecord.records.concat(newRecord);
        origCheckoutListRecord.records = origCheckoutListRecord.records.concat(newRecord);
      } else if (hasPending) {
        // add brand new record
        const newRecord = this.createProcessRequestRecord(v);
        this.originalCheckOutList = this.maintainIsCheckedInFlag(this.originalCheckOutList.concat(newRecord));
        if (this.view === 'check-out') {
          this.checkOutList = this.maintainIsCheckedInFlag(this.checkOutList.concat(newRecord));
        }
      }

    });
  }

  /**
   * onCheckboxChange
   *
   * @memberof ProcessInventoryRequestComponent
   */
  public onCheckboxChange() {
    this.displayCheckOutBtn = this.hasCheckedOutItem || this.hasCheckedInItem;
  }

  public onCheckboxChangeAll(item: ProcessInventoryRecord) {
    const index = this.checkOutList.findIndex(c => c.id === item.id);
    let records: InventoryRecord[];

    if (this.view === 'check-out') {
      records = this.checkOutList[index].records.map(r => ({ ...r, isCheckedOut: item.allChecked }));
    } else {
      records = this.checkOutList[index].records.map(r => ({ ...r, isCheckedIn: item.allChecked }));
    }
    this.checkOutList[index] = { ...this.checkOutList[index], records };
    this.displayCheckOutBtn = this.hasCheckedOutItem || this.hasCheckedInItem;

  }

  /**
   * checkInOrOut
   *
   * @param {('out' | 'in')} type
   * @memberof ProcessInventoryRequestComponent
   */
  public checkInOrOut(type: 'out' | 'in' | 'broken') {
    switch (type) {
      case 'out':
        this.checkOutItems();
        break;

      default:
        this.checkInItems(type);
        break;
    }
  }

  /**
   * Handle Segment changed
   *
   * @memberof ProcessInventoryRequestComponent
   */
  public onSegmentChanged() {
    // this.getCheckOutItems();
    switch (this.view) {
      case 'check-out':
        this.segmentCheckOut();
        break;
      case 'check-in':
        this.segmentCheckIn();
        break;
      case 'broken':
        this.segmentBroken();
        break;
    }
  }

  /**
   * onSearch
   *
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  public onSearch() {
    const originalListProp = this.view === 'check-out' ? 'originalCheckOutList' : 'originalCheckInList';

    of(this[originalListProp])
      .pipe(
        take(1),
        switchAll(),
        map(v => {
          const newObj = { ...v };
          newObj.records = newObj.records.filter(r => r.name.toLowerCase().includes(this.searchInput.toLowerCase()));
          return newObj;
        }),
        toArray(),
        tap(v => this.checkOutList = v)
      )
      .subscribe();
  }

  /**
  * Display popover menu
  *
  * @param {Event} ev
  * @memberof InventoryListComponent
  */
  public async presentPopoverMenu(ev: Event, id: string, itemId: string) {
    const popover = await this.popoverController.create({
      component: PopoverMenuComponent,
      event: event,
      mode: 'md',
      componentProps: {
        list: [
          { name: 'Update Check-In Qty', value: 'update-checkin-qty', icon: 'sync', show: this.view === 'check-in' },
          { name: 'Broken', value: 'broken', icon: 'build', show: this.view === 'check-in' },
          { name: 'Update Quantity', value: 'update-qty', icon: 'sync', show: this.view === 'check-out' },
          { name: 'Reject', value: 'reject', icon: 'close-circle-outline', show: this.view === 'check-out' },
        ]
      }
    });

    await popover.present();

    const v = await popover.onDidDismiss();

    if (v.data && v.data !== null) {
      const checkOutItem = this.checkoutQuery.getEntity(id);
      const checkedOutInventoryItem = checkOutItem.items.find(i => i.id === itemId && i.type !== 'checkin');
      const inventoryItem = this.inventoryQuery.getEntity(itemId);
      const activeUser = this.userQuery.getActive();
      const user = this.userQuery.getEntity(checkOutItem.userId);
      let quantity: { data: number };
      this.checkoutService.setActive(checkOutItem.id);
      switch (v.data.item) {
        case 'reject':
          if (activeUser.id === user.id) {
            this.presentToast('Error: Remove your own inventory items under Inventory.');
            return;
          }
          this.inventoryService.updateAvails([{ ...checkedOutInventoryItem, type: 'remove' }]);
          this.checkoutService.createOrReplace({ ...checkedOutInventoryItem, type: 'remove' }, user, true);
          this.checkoutService.markPristene([itemId]);
          break;
        case 'update-qty':
          const existingQuantity = checkedOutInventoryItem.quantity;
          quantity = await this.quantitySelectorPopover('Update Quantity', ev, 0, true);
          let isAvailable = false;

          // check available amount if needed
          if (!inventoryItem.allowZeroQty) {
            isAvailable = this.inventoryService.updateAvailAndQuantity(inventoryItem, quantity.data, existingQuantity || 0);
          }

          if (isAvailable || inventoryItem.allowZeroQty) {
            this.checkoutService.updateUserQuantity(checkedOutInventoryItem, quantity.data);
            const available = inventoryItem.available + existingQuantity - quantity.data;
            this.inventoryService.upsert({ ...inventoryItem, available });
          }
          break;

        case 'update-checkin-qty':
          quantity = await this.quantitySelectorPopover('Update Quantity', ev, 0, true);
          this.checkOutList.
            find(c => c.id === id).records.
            find(r => r.id === itemId).
            quantity = quantity.data;
          break;

        case 'broken':
          await this.checkoutService.updateAllCheckOutType(checkOutItem.id);
          this.inventoryService.markBroken(checkedOutInventoryItem.id);
          break;

        default:
          break;
      }
    }
  }

  /*************************************
   *
   * Private Methods
   *
   *************************************/

  /**
   * Handle segment change event for CheckOut
   *
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private segmentCheckOut() {
    of(this.originalCheckOutList)
      .pipe(
        take(1),
        switchAll(),
        map(v => {
          const newObj = { ...v };
          newObj.records = newObj.records.filter(r => {
            return r.name.toLowerCase().includes(this.searchInput.toLowerCase()) &&
              this.checkoutService.isPending(v.id, r.id);
          });
          return newObj;
        }),
        filter(v => !!v.records.length),
        toArray(),
        tap(v => this.checkOutList = v)
      )
      .subscribe();
  }

  /**
   * Handle segment change event for CheckIn
   *
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private segmentCheckIn() {
    of(this.originalCheckInList)
      .pipe(
        take(1),
        switchAll(),
        map(v => {
          const newObj = { ...v };
          newObj.records = newObj.records.filter(r => {
            return r.name.toLowerCase().includes(this.searchInput.toLowerCase()) &&
              this.checkoutService.isCheckedOut(v.id, r.id);
          });
          return newObj;
        }),
        filter(v => !!v.records.length),
        toArray(),
        tap(v => this.checkOutList = v)
      )
      .subscribe();
  }

  // broken segment button
  private segmentBroken() {
    of(this.originalBrokenList)
      .pipe(
        take(1),
        switchAll(),
        map(v => {
          const newObj = { ...v };
          newObj.records = newObj.records.filter(r => {
            return r.name.toLowerCase().includes(this.searchInput.toLowerCase()) &&
              this.checkoutService.isBroken(v.id, r.id);
          });
          return newObj;
        }),
        filter(v => !!v.records.length),
        toArray(),
        tap(v => this.checkOutList = v)
      )
      .subscribe();
  }

  /**
   * Load checkout and checkin items
   *
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private getCheckOutItems() {
    this.checkoutQuery.selectAll({ filterBy: c => c.items === undefined ? false : c.items.some(i => !['checkin', 'remove'].includes(i.type)) })
      .pipe(
        filterNil,
        take(1),
        switchAll(),
        map<CheckoutItem, ProcessInventoryRecord>(this.createProcessRequestRecord),
        reduce((acc, v) => {
          const checkOut = { ...v };
          const checkIn = { ...v };
          const broken = { ...v };

          checkOut.records = checkOut.records.filter(r => {
            return r.name.toLowerCase().includes(this.searchInput.toLowerCase()) &&
              this.checkoutService.isPending(v.id, r.id);
          });

          checkIn.records = checkIn.records.filter(r => {
            return r.name.toLowerCase().includes(this.searchInput.toLowerCase()) &&
              this.checkoutService.isCheckedOut(v.id, r.id);
          });

          broken.records = broken.records.filter(r => {
            return r.name.toLowerCase().includes(this.searchInput.toLowerCase()) &&
              this.checkoutService.isBroken(v.id, r.id);
          });

          if (!!checkOut.records.length) {
            acc.checkOut = acc.checkOut.concat(checkOut);
          }

          if (!!checkIn.records.length) {
            acc.checkIn = acc.checkIn.concat(checkIn);
          }

          if (!!broken.records.length) {
            acc.broken = acc.broken.concat(broken);
          }

          return acc;
        }, { checkOut: [], checkIn: [], broken: [] }),
        tap(v => {
          let prop: string;

          switch (this.view) {
            case 'check-out':
              prop = 'checkOut';
              break;
            case 'check-in':
              prop = 'checkIn';
              break;
            case 'broken':
              prop = 'broken';
              break;
          }

          this.checkOutList = !this.checkOutList.length ? v[prop] : this.maintainIsCheckedInFlag(v[prop]);

          this.originalCheckOutList = v.checkOut;
          this.originalCheckInList = v.checkIn;
          this.originalBrokenList = v.broken;
        })
      )
      .subscribe();
  }

  /**
   * maintainIsCheckedInFlag
   *
   * @private
   * @param {ProcessInventoryRecord[]} v
   * @returns
   * @memberof ProcessInventoryRequestComponent
   */
  private maintainIsCheckedInFlag(v: ProcessInventoryRecord[]) {
    return v.map((a) => {
      a.records = a.records.map(r => {
        const item = this.checkOutList.slice(0).find(c => c.id === a.id);
        const record = item ? item.records.find(c => c.id === r.id) : undefined;
        return record ? { ...r, isCheckedIn: record.isCheckedIn, isCheckedOut: record.isCheckedOut } : r;
      });
      return a;
    });
  }

  /**
   * createProcessRequestRecord
   *
   * @private
   * @param {CheckoutItem} checkOutItem
   * @returns
   * @memberof ProcessInventoryRequestComponent
   */
  private createProcessRequestRecord = (checkOutItem: CheckoutItem): ProcessInventoryRecord => {
    const user = this.userQuery.getEntity(checkOutItem.userId);
    const records = checkOutItem.items === undefined ? [] : checkOutItem.items
      .reduce((acc, p) => {
        if (!['checkin', 'remove'].includes(p.type) && this.inventoryQuery.hasEntity(p.id)) {
          const item = this.inventoryQuery.getEntity(p.id);
          const isCheckedOut = p.type === 'checkout-pending';
          const date = checkOutItem.date;
          const record = {
            ...item,
            quantity: p.quantity,
            isCheckedOut,
            date,
            status: p.type,
            truck: p.truckName
          };
          return acc.concat(record);
        }
        return acc;
      }, []);
    return { name: user.fullName, id: checkOutItem.id, date: checkOutItem.date, records, allChecked: false, userId: user.id };
  }

  private getProcessRequestRecord(checkoutItem: ItemIdData, date) {
    const item = this.inventoryQuery.getEntity(checkoutItem.id);
    const isCheckedOut = checkoutItem.type === 'checkout-pending';
    return {
      ...item,
      quantity: checkoutItem.quantity,
      isCheckedOut,
      date,
      status: checkoutItem.type,
      truck: checkoutItem.truckName
    };
  }

  /**
   * Handle check-out items request
   *
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private checkOutItems() {
    this.checkOutList.forEach(async item => {
      item.records
        .forEach(async inventoryItem => {
          if (inventoryItem.isCheckedOut) {
            const user = this.userQuery.getEntity(item.userId);
            this.checkoutService.setActive(item.id);
            const checkoutItem = this.checkoutQuery.getEntity(item.id);
            const checkoutInventoryItem = checkoutItem.items.find(i => i.id === inventoryItem.id && i.type !== 'checkin');
            if (inventoryItem.consumable) {
              this.inventoryService.uncheckItem(inventoryItem.id);
              this.checkoutService.createOrReplace({ ...checkoutInventoryItem, type: 'checkin' }, user, true);

              await this.checkoutService.updateInventoryForCheckIn(item.id, inventoryItem.id, 'checkin');
            } else {
              const type = inventoryItem.partialCml ? 'partial' : 'checkout';
              const currentUser = this.userQuery.getActive();
              if (item.userId === currentUser.id) {
                this.inventoryService.updateInventoryStatus(inventoryItem.id, type);
              }
              this.checkoutService.createOrReplace({ ...checkoutInventoryItem, type }, user, true);
            }
          }
        });

      if (item.records.some(i => i.isCheckedOut)) {
        await this.checkoutService.updateAllCheckOutType(item.id);
      }
    });

    this.displayCheckOutBtn = false;

    this.segmentCheckOut();
  }

  /**
   * Handle check-in items request
   *
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private checkInItems(type = 'in') {
    this.checkOutList.forEach(async item => {
      item.records
        .forEach(async (inventoryItem) => {
          if (inventoryItem.isCheckedIn) {
            // update quantity for checked in item
            const user = this.userQuery.getEntity(item.userId);
            const checkoutItem = this.checkoutQuery.getEntity(item.id);
            const checkoutInventoryItem = checkoutItem.items.find(i => i.id === inventoryItem.id);
            this.checkoutService.setActive(item.id);
            this.checkoutService.createOrReplace({ ...checkoutInventoryItem, quantity: inventoryItem.quantity, type: 'checkin' }, user, true);

            this.inventoryService.uncheckItem(inventoryItem.id, type === 'broken');
            await this.checkoutService.updateInventoryForCheckIn(item.id, inventoryItem.id, 'checkin');
          }
        });

      if (item.records.some(i => i.isCheckedIn)) {
        await this.checkoutService.updateAllCheckOutType(item.id);
      }
    });

    this.displayCheckOutBtn = false;

    if (type === 'broken') {
      this.segmentBroken();
      return;
    }
    this.segmentCheckIn();
  }

  /**
   * hasCheckedOutItem
   *
   * @readonly
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private get hasCheckedOutItem() {
    return this.checkOutList.some(item => {
      return item.records
        .some(v => v.isCheckedOut);
    });
  }

  /**
   * hasCheckedInItem
   *
   * @readonly
   * @private
   * @memberof ProcessInventoryRequestComponent
   */
  private get hasCheckedInItem() {
    return this.checkOutList.some(item => {
      return item.records
        .some(v => v.isCheckedIn);
    });
  }

  /**
   * Quantity selector popover
   *
   * @private
   * @param {string} [title='Quantity']
   * @param {Event} ev
   * @returns
   * @memberof InventoryListComponent
   */
  private async quantitySelectorPopover(title: string = 'Quantity', ev: Event, quantity = 0, backdropDismiss = false) {
    const popover = await this.popoverController.create({
      component: PopoverQuantitySelectionComponent,
      event: ev,
      componentProps: { title, quantity },
      backdropDismiss: quantity === 0 || backdropDismiss ? true : false
    });

    await popover.present();

    return await popover.onDidDismiss() as { data: number };
  }

  /**
   * Present toast
   *
   * @private
   * @param {*} message
   * @memberof InventoryListComponent
   */
  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

}
