import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProcessInventoryRequestComponent } from './process-inventory-request.component';

describe('ProcessInventoryRequestComponent', () => {
  let component: ProcessInventoryRequestComponent;
  let fixture: ComponentFixture<ProcessInventoryRequestComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProcessInventoryRequestComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProcessInventoryRequestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
