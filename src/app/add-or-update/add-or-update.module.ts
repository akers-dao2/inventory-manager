import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AddOrUpdateComponent } from './add-or-update.component';
import { SharedModules } from '../shared/modules/shared.modules';

@NgModule({
  imports: [
    SharedModules,
    RouterModule.forChild([
      {
        path: '',
        component: AddOrUpdateComponent
      }
    ])
  ]
})
export class AddOrUpdateComponentModule {}
