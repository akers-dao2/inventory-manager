import { Component, OnInit, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { InventoryService, createInventoryItem, InventoryItem } from '../shared/tables/state';
import { ToastController, ModalController } from '@ionic/angular';

@Component({
  selector: 'app-add-or-update',
  templateUrl: 'add-or-update.component.html',
  styleUrls: ['add-or-update.component.scss'],
})
export class AddOrUpdateComponent implements OnInit {
  public addItemForm: FormGroup;
  public isModal = false;
  @Input() entity: InventoryItem;
  constructor(
    private fb: FormBuilder,
    private inventoryService: InventoryService,
    private toastController: ToastController,
    public modalController: ModalController,
  ) { }

  ngOnInit() {
    this.createForm();

    if (this.entity) {
      this.isModal = true;
      let rest: Partial<InventoryItem>,
        isChecked: boolean,
        isCheckedOut: boolean,
        isCheckedIn: boolean,
        status: any,
        quantity: any,
        broken: any;
      ({ isChecked, status, quantity, isCheckedIn, isCheckedOut, broken, ...rest } = this.entity);
      rest = this.addMissingProp(rest, [
        { name: 'allowZeroQty', default: false },
        { name: 'allowRepairs', default: false },
        { name: 'hideInventoryReport', default: false },
        { name: 'partialCml', default: false },
        { name: 'order', default: 0 },
        { name: 'reorder', default: 0 },
      ]);
      this.addItemForm.setValue(rest);
    }
  }

  private addMissingProp(inventoryItem: Partial<InventoryItem>, props: { name: string, default: any }[]) {
    props.forEach(prop => {
      if (inventoryItem[prop.name] === undefined) {
        inventoryItem = { ...inventoryItem, [prop.name]: prop.default };
      }
    });

    return inventoryItem;
  }

  /**
   * Handle close event
   *
   * @memberof AddComponent
   */
  public onClose() {
    this.modalController.dismiss();
  }

  /**
   * Update or Insert inventory item
   *
   * @memberof AddComponent
   */
  public upsertItem() {
    const values = this.addItemForm.value;

    if (this.isModal) {
      this.inventoryService.upsert(values);
      this.modalController.dismiss();
    } else {
      this.inventoryService.upsert(createInventoryItem(values));
      this.addItemForm.reset();
      this.presentToast('User Add Successfully');
    }

  }

  /**
   * Create a new form
   *
   * @private
   * @memberof AddComponent
   */
  private createForm() {
    this.addItemForm = this.fb.group({
      id: [''],
      location: ['', Validators.required],
      name: ['', Validators.required],
      standard: [, Validators.required],
      available: [, Validators.required],
      reorder: [0, Validators.required],
      consumable: [false],
      vendor: [''],
      order: [''],
      allowZeroQty: [false],
      partialCml: [false],
      allowRepairs: [false],
      hideInventoryReport: [false],
    });
  }

  /**
   * Present toast message
   *
   * @private
   * @param {string} message
   * @memberof AddComponent
   */
  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }
}
