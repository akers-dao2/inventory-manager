import { Component, OnInit, Output, EventEmitter, OnChanges, SimpleChanges, Input } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { filter, debounceTime, tap, map } from 'rxjs/operators';
import * as moment from 'moment';
import { PopoverController } from '@ionic/angular';
import { PopoverFilterComponent } from '../../shared/components/popover-filter/popover-filter.component';
import { Subject } from 'rxjs';


@Component({
  selector: 'app-filter-panel',
  templateUrl: './filter-panel.component.html',
  styleUrls: ['./filter-panel.component.scss']
})
export class FilterPanelComponent implements OnInit, OnChanges {
  public showFilterPanel = false;
  public form: FormGroup = this.fb.group(
    {
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      name: [undefined],
      user: [undefined],
      truck: [undefined],
      status: [undefined],
      location: [undefined],
      vendor: [undefined],
      consumable: [undefined],
      reorder: [undefined],
    }
  );
  public filterPopover$ = new Subject();
  @Output() valueChanges = new EventEmitter();
  @Input() trigger: boolean;
  @Input() fields: string[] = [];
  @Input() filterData: {}[] = [];

  constructor(
    private fb: FormBuilder,
    private popoverController: PopoverController,
  ) { }

  ngOnInit() {
    this.setCurrentStateAndEndDate();

    this.showFields(this.fields);

    this.form.valueChanges
      .pipe(
        filter(() => this.form.valid),
        debounceTime(500),
        map(o =>
          Object.keys(o)
            .filter(k => this.fields.includes(k) && o[k] !== null)
            .reduce((acc, k) => {
              return k === 'startDate' || k === 'endDate' ? { ...acc, [k]: moment(o[k], moment.HTML5_FMT.DATE).valueOf() } : { ...acc, [k]: o[k] };
            }, {})
        ),
        tap(o => this.valueChanges.emit({ value: o }))
      )
      .subscribe();

    this.filterPopover$
      .pipe(
        debounceTime(500),
        tap<{ event: any, name: string }>(async v => {
          const resp = await this.filterPopover(v.event, v.name);
          if (resp.data) {
            this.form.get(v.name).patchValue(resp.data);
          }
        })
      )
      .subscribe();

  }

  /**
   * showFields
   * @param fields
   */
  public showFields(fields: string[]) {
    fields.forEach(name => this[name] = true);
  }

  /**
   * ngOnChanges
   * @param changes
   */
  ngOnChanges(changes: SimpleChanges) {
    for (const propName in changes) {
      if (changes.hasOwnProperty(propName) && propName === 'trigger') {
        const chng = changes[propName];
        if (chng.currentValue) {
          this.form.updateValueAndValidity({ onlySelf: false, emitEvent: true });
        }
      }
    }
  }

  /**
   * setCurrentStateAndEndDate
   *
   *
   */
  public setCurrentStateAndEndDate() {
    const startDate = moment().date(1).toJSON();
    const numberOfDays = moment().daysInMonth();
    const endDate = moment().date(numberOfDays).toJSON();
    this.form.patchValue({ startDate, endDate });
  }

  /**
   * filterPopover
   */
  public async filterPopover(ev: Event, prop: string) {
    const names = this.filterData
      .map(f => ({ name: f[prop] || f['reorderStatus']  || f['name'], value: f[prop] || f['reorderStatus'] || f['name'] }))
      .filter(f => f.name.toString().toUpperCase().includes(this.form.value[prop].toString().toUpperCase()))
      .reduce((acc, f) => {
        const hasMatch = acc.some(v => v.name === f.name);
        return hasMatch ? acc : [...acc, f];
      }, []);

    const popover = await this.popoverController.create({
      component: PopoverFilterComponent,
      event: ev,
      componentProps: { names },
      showBackdrop: false,
      mode: 'md',
      translucent: true
    });

    await popover.present();

    return await popover.onDidDismiss() as { data: string };
  }

}
