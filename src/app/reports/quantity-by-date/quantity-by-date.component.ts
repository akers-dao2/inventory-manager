import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { tap, filter, switchAll, switchMap, map, take, toArray, mergeMap, debounceTime, groupBy, reduce } from 'rxjs/operators';
import { InventoryQuery, CheckoutQuery, ItemIdData } from '../../shared/tables/state';
import * as moment from 'moment';

@Component({
  selector: 'app-quantity-by-date',
  templateUrl: './quantity-by-date.component.html',
  styleUrls: ['./quantity-by-date.component.scss']
})
export class QuantityByDateComponent implements OnInit {
  public form: FormGroup;
  public inventory: { name: string, quantity: number }[] = [];
  public show = false;

  constructor(
    private fb: FormBuilder,
    private inventoryQuery: InventoryQuery,
    private checkoutQuery: CheckoutQuery
  ) { }

  ngOnInit() {
    this.createForm();
    this.setCurrentStateAndEndDate();

    this.form.valueChanges
      .pipe(
        filter(() => this.form.valid),
        debounceTime(500),
        tap(this.getInventory)
      )
      .subscribe();
  }

  /**
   * setCurrentStateAndEndDate
   *
   * @memberof QuantityByDateComponent
   */
  public setCurrentStateAndEndDate() {
    const startDate = moment().date(1).toJSON();
    const numberOfDays = moment().daysInMonth();
    const endDate = moment().date(numberOfDays).toJSON();
    this.form.patchValue({ startDate, endDate });
  }

  /**
   * getInventory
   *
   * @private
   * @memberof QuantityByDateComponent
   */
  private getInventory = (formData: { startDate: string, endDate: string, name: string }) => {
    const startDate = this.getNumericalValue(formData.startDate);
    const endDate = this.getNumericalValue(formData.endDate);
    const isConsumable = filter(this.isConsumable);
    const getInventoryItemWithQuantity = map(this.getInventoryItemWithQuantity);
    const filterByName = filter<any>(i => i.name.toUpperCase().includes(formData.name.toUpperCase()));

    this.checkoutQuery
      .selectAll({ filterBy: c => moment(c.date).isBetween(startDate, endDate) })
      .pipe(
        take(1),
        switchAll<any>(),
        switchMap(c => c.items ? c.items : []),
        isConsumable,
        getInventoryItemWithQuantity,
        filterByName,
        groupBy(i => i.name),
        mergeMap(i => {
          return i.pipe(
            reduce((acc, v) => {
              return { ...acc, name: v.name, quantity: v.quantity + acc.quantity };
            }, { name: '', quantity: 0 })
          );

        }),
        toArray(),
        tap(console.log),
        tap(i => this.inventory = i)
      )
      .subscribe();
  }

  /**
   * isConsumable
   *
   * @private
   * @memberof QuantityByDateComponent
   */
  private isConsumable = (item: ItemIdData) => {
    return this.inventoryQuery.getEntity(item.id).consumable;
  }

  /**
   * getInventoryItemWithQuantity
   *
   * @private
   * @memberof QuantityByDateComponent
   */
  private getInventoryItemWithQuantity = (item: ItemIdData) => {
    const inventoryItem = this.inventoryQuery.getEntity(item.id);
    return { ...inventoryItem, quantity: item.quantity };
  }

  /**
   * getNumericalValue
   *
   * @private
   * @memberof QuantityByDateComponent
   */
  private getNumericalValue = (date: string) => {
    return new Date(date).valueOf();
  }

  /**
   * Create a new form
   *
   * @private
   * @memberof AddComponent
   */
  private createForm() {
    this.form = this.fb.group({
      startDate: ['', Validators.required],
      endDate: ['', Validators.required],
      name: ['', Validators.required],
    });
  }

}
