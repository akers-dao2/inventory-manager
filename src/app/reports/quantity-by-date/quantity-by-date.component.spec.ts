import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { QuantityByDateComponent } from './quantity-by-date.component';

describe('QuantityByDateComponent', () => {
  let component: QuantityByDateComponent;
  let fixture: ComponentFixture<QuantityByDateComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ QuantityByDateComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(QuantityByDateComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
