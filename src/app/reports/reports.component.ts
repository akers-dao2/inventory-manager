import { Component, OnInit, OnDestroy } from '@angular/core';
import { NavigationComponent } from './navigation/navigation.component';
import { Router, NavigationEnd } from '@angular/router';
import { map, takeUntil, filter } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { PrintService } from '../shared/services/print.service';

@Component({
  selector: 'app-reports',
  templateUrl: './reports.component.html',
  styleUrls: ['./reports.component.scss']
})
export class ReportsComponent implements OnInit, OnDestroy {
  public startPage = NavigationComponent;
  public showBackBtn = false;
  public showPrintBtn = false;
  public unSubscribe = new Subject();
  constructor(
    private router: Router,
    private printService: PrintService
  ) { }

  ngOnInit() {
    this.showBackBtn = this.router.url.includes('/reports/');
    this.showPrintBtn = this.router.url.includes('/reports/');

    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd),
        map((e: NavigationEnd) => {
          this.showBackBtn = e.url.includes('/reports/');
          this.showPrintBtn = this.router.url.includes('/reports/');
        }),
        takeUntil(this.unSubscribe),
      )
      .subscribe();
  }


  public ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  public print() {
    this.printService.printPage(this.router.url);
  }

}
