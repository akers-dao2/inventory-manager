import { Component, OnInit, OnDestroy } from '@angular/core';
import { InventoryQuery, InventoryItem } from '../../shared/tables/state';
import { PopoverController, ToastController } from '@ionic/angular';
import { PopoverFilterComponent } from '../../shared/components/popover-filter/popover-filter.component';
import { FilterPanelFormData } from '../../shared/interfaces/filter-panel-form-data';
import { auditTime, tap, bufferCount, switchAll, take, map, toArray, filter, takeUntil } from 'rxjs/operators';
import { Subject } from 'rxjs';
import { SortInventoryService } from '../../shared/services/sort-inventory.service';
import { Router } from '@angular/router';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { PrintService } from '../../shared/services/print.service';

@Component({
  selector: 'app-inventory-report',
  templateUrl: './inventory-report.component.html',
  styleUrls: ['./inventory-report.component.scss']
})
export class InventoryReportComponent implements OnInit, OnDestroy {
  public reports: InventoryItem[][];
  public reportsData: InventoryItem[];
  public origReportsData: InventoryItem[] = [];
  public filterData: InventoryItem[] = [];
  public fields = ['location', 'name', 'vendor', 'consumable', 'status'];
  private cache = new Map<string, string>();
  private toast: HTMLIonToastElement;
  private listIndex = 0;
  public skeletonList = Array(100).fill('');
  private isSearching = false;
  public unSubscribe = new Subject();

  constructor(
    private inventoryQuery: InventoryQuery,
    private popoverController: PopoverController,
    private toastController: ToastController,
    private sortBy: SortInventoryService,
    private router: Router,
    private printService: PrintService
  ) { }

  ngOnInit() {

    this.inventoryQuery.selectAll()
      .pipe(
        auditTime(0),
        filter(d => !!d.length),
        this.sortBy.name,
        map(d => this.addReorder(d)),
        take(1),
        switchAll(),
        filter(d => !d.hideInventoryReport),
        bufferCount(25),
        toArray(),
        tap(d => {
          this.reports = d;
          this.reportsData = d[this.listIndex];
          this.origReportsData = d.reduce((acc, v) => [...acc, ...v], []);
          this.filterData = this.origReportsData.map(o => {
            return { ...o, status: o.reorderStatus };
          }) as any;
        })
      )
      .subscribe();

    this.printService.onPrint
      .pipe(
        filter(url => url === '/reports/inventory-report'),
        tap(this.print),
        takeUntil(this.unSubscribe)
      )
      .subscribe();

  }

  public ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }

  public loadMore(event) {
    const numRows = this.reports.length - 1;
    if (numRows === this.listIndex || this.isSearching) {
      event.target.disabled = true;
      return;
    }
    this.reportsData = this.reportsData.concat(this.reports[++this.listIndex]);
    event.target.complete();
  }

  public async filter($event: Event, type: string) {

    switch (type) {
      case 'name':
        this.filterByProp($event, 'name');
        break;
      case 'location':
        this.filterByProp($event, 'location');
        break;
      case 'vendor':
        this.filterByProp($event, 'vendor');
        break;
      case 'consumable':
        this.filterByProp($event, 'consumable');
        break;
      case 'reorder':
        this.filterByProp($event, 'reorderStatus');
        break;
    }
  }

  valueChanges({ value }: { value: FilterPanelFormData }) {
    Object.keys(value)
      .forEach(key => {
        let type: string;

        switch (key) {
          case 'status':
            type = 'reorderStatus';
            break;

          default:
            type = key;
            break;
        }
        this.isSearching = true;
        this.filterList({ data: value[key] }, type);
      });
  }

  print = () => {
    const prop = !this.cache.size ? 'origReportsData' : 'reportsData';
    const doc = new jsPDF();
    const totalPagesExp = '{total_pages_count_string}';
    doc.autoTable({
      margin: { top: 15, right: 1, bottom: 5, left: 1 },
      head: [
        { location: 'Location', name: 'Name', available: 'Avail', standard: 'Standard', reorder: 'Reorder', consumable: 'Consumable', status: 'Status', vendor: 'Vendor' },
      ],
      body: this[prop].map(d => {
        return {
          location: d.location,
          name: d.name,
          available: d.available,
          standard: d.standard,
          reorder: d.reorder,
          consumable: d.consumable,
          status: d.reorderStatus,
          vendor: d.vendor
        };
      }),
      columnStyles: {
        location: {
          minCellWidth: 20
        },
        name: {
          minCellWidth: 55
        },
        available: {
          minCellWidth: 15
        },
        standard: {
          minCellWidth: 20
        },
        reorder: {
          minCellWidth: 17
        },
        consumable: {
          minCellWidth: 25
        },
        status: {
          minCellWidth: 22
        },
        vendor: {
          minCellWidth: 30
        },
      },
      didDrawPage: function (data) {
        // Header
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        doc.text('Inventory Report', data.settings.margin.left, 10);

        // Footer
        let str = 'Page ' + doc.internal.getNumberOfPages();
        if (typeof doc.putTotalPages === 'function') {
          str = str + ' of ' + totalPagesExp;
        }
        doc.setFontSize(7);

        const pageSize = doc.internal.pageSize;
        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight - 3);
      },

    });

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }
    doc.save('table.pdf');
  }

  /*************************************
   *
   * Private Methods
   *
   *************************************/

  /**
   *
   *
   * @private
   * @param {InventoryItem[]} reportsData
   * @returns
   * @memberof InventoryReportComponent
   */
  private addReorder(reportsData: InventoryItem[]) {
    return reportsData.map(r => {
      return {
        ...r,
        reorderStatus: r.available <= (r.reorder || 0) ? 'REORDER' : 'GOOD'
      } as InventoryItem;
    });
  }

  /**
   * filterByProp
   *
   * @private
   * @param {*} $event
   * @param {*} prop
   * @memberof InventoryReportComponent
   */
  private async filterByProp($event: Event, prop: string) {
    const results = this.origReportsData
      .map(r => ({ name: r[prop], value: r[prop] }))
      .filter(r => r.name !== undefined && r.name !== '')
      .reduce((acc: { name: string, value: string }[], r: { name: string, value: string }) => {
        return acc.some(a => this.trim(a.name) === this.trim(r.name)) ? acc : acc.concat(r);
      }, [])
      .sort(this.sort.bind(this, 'name'));


    results.unshift({ name: 'All', value: 'all' });
    const result = await this.filterPopover($event, results);
    this.filterList(result, prop);
  }

  /**
   * filterList
   *
   * @private
   * @param {{ data: string }} result
   * @param {string} type
   * @returns
   * @memberof InventoryReportComponent
   */
  private filterList(result: { data: string }, type: string) {
    if (result.data === undefined) {
      return;
    }

    if (this.cache.has(type) && this.cache.get(type) === result.data) {
      return;
    }

    this.reportsData = this.reloadExistFilters(type, this.origReportsData);

    // remove filter if data equal ''
    if (result.data === '') {
      return;
    }

    this.reportsData = this.reportsData
      .filter(d => d[type] !== null)
      .filter(d => d[type].toString().toUpperCase().includes(result.data.toString().toUpperCase()));
    this.cache.set(type, result.data);
  }

  /**
   * sort
   *
   * @private
   * @param {string} prop
   * @param {{}} a
   * @param {{}} b
   * @returns
   * @memberof InventoryReportComponent
   */
  private sort(prop: string, a: {}, b: {}) {
    const nameA = this.toUpperCase(a[prop]);
    const nameB = this.toUpperCase(b[prop]);
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;

  }

  /**
   * trim
   *
   * @private
   * @param {(string | boolean)} value
   * @returns
   * @memberof InventoryReportComponent
   */
  private trim(value: string | boolean) {
    if (typeof value === 'string') {
      return value.trim();
    }

    return value;
  }

  /**
   * toUpperCase
   *
   * @private
   * @param {(string | boolean)} value
   * @returns
   * @memberof InventoryReportComponent
   */
  private toUpperCase(value: string | boolean) {
    if (typeof value === 'string') {
      return value.toUpperCase();
    }

    return value;
  }

  /**
   * Filter selector popover
   *
   * @private
   * @param {string[]} names
   * @param {Event} ev
   * @returns
   * @memberof ReportComponent
   */
  private async filterPopover(ev: Event, names: any[]) {
    const popover = await this.popoverController.create({
      component: PopoverFilterComponent,
      event: ev,
      componentProps: { names }
    });

    await popover.present();

    return await popover.onDidDismiss() as { data: string };
  }

  /**
   * getResetFilterForType
   *
   * @private
   * @param {string} type
   * @param {InventoryItem[]} origReportsData
   * @returns
   * @memberof InventoryReportComponent
   */
  private reloadExistFilters(type: string, origReportsData: InventoryItem[]) {
    let reportsData = origReportsData.slice(0);

    if (this.cache.has(type)) {
      this.cache.delete(type);
    }

    this.cache.forEach((v, k) => {
      reportsData = reportsData
        .filter(r => r[k] !== null)
        .filter(r => r[k].toString().toUpperCase().includes(v.toString().toUpperCase()));
    });

    return reportsData;
  }
}
