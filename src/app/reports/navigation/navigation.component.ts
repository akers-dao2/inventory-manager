import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.scss']
})
export class NavigationComponent implements OnInit {
  public reports: { name: string, url: string }[] = [];

  constructor() { }

  ngOnInit() {
    this.reports = [
      { name: 'User Report', url: '/reports/users-report' },
      { name: 'Inventory Report', url: '/reports/inventory-report' },
      { name: 'Quantity By Date Report', url: '/reports/quantity-by-date-report' }
    ];
  }

}
