import { Component, OnInit, OnDestroy } from '@angular/core';
import { InventoryQuery, UsersQuery, CheckoutItem, CheckoutQuery } from '../../shared/tables/state';
import { ProcessInventoryRecord, InventoryRecord } from '../../shared/interfaces/process-inventory-record';
import { from, of, combineLatest, Subject } from 'rxjs';
import { map, toArray, tap, filter, groupBy, mergeMap, reduce, take, switchMap, auditTime, delay, takeUntil } from 'rxjs/operators';
import { PopoverController, ToastController } from '@ionic/angular';
import { PopoverFilterComponent } from '../../shared/components/popover-filter/popover-filter.component';
import { DatePipe } from '@angular/common';
import { Router, NavigationEnd } from '@angular/router';
import { FilterPanelFormData } from '../../shared/interfaces/filter-panel-form-data';
import * as moment from 'moment';
import jsPDF from 'jspdf';
import 'jspdf-autotable';
import { PrintService } from '../../shared/services/print.service';

@Component({
  selector: 'app-report',
  templateUrl: './report.component.html',
  styleUrls: ['./report.component.scss']
})
export class ReportComponent implements OnInit, OnDestroy {
  public reportsData: ProcessInventoryRecord[] = [];
  public origReportsData: ProcessInventoryRecord[] = [];
  public filterData: ProcessInventoryRecord[] = [];
  private cache = new Map<string, string>();
  private toast: HTMLIonToastElement;
  private datePipe = new DatePipe('en-US');
  public triggerFilters = false;
  public fields = ['truck', 'name', 'status', 'startDate', 'endDate', 'user'];
  public unSubscribe = new Subject();


  constructor(
    private inventoryQuery: InventoryQuery,
    private userQuery: UsersQuery,
    private checkoutQuery: CheckoutQuery,
    private popoverController: PopoverController,
    private toastController: ToastController,
    private router: Router,
    private printService: PrintService
  ) { }

  ngOnInit() {
    const sortRecords$ = map((i: { records: any[] }) => {
      i.records.sort((a, b) => {
        const nameA = a.date;
        const nameB = b.date;
        if (nameA > nameB) {
          return -1;
        }
        if (nameA < nameB) {
          return 1;
        }
        return 0;
      });

      return i;
    });

    combineLatest(
      this.checkoutQuery.selectAll(),
      this.inventoryQuery.selectAll()
    )
      .pipe(
        auditTime(0),
        filter(([c, i]) => !!c.length && !!i.length),
        take(1),
        switchMap(([cc]) =>
          from(cc)
            .pipe(
              map(c => this.createRecord(c as CheckoutItem)),
              filter(c => c !== null),
              groupBy(s => s.name),
              mergeMap(s => {
                return s
                  .pipe(
                    reduce((acc: ProcessInventoryRecord, c: ProcessInventoryRecord) => {
                      if ('records' in acc) {
                        acc.records = [...acc.records, ...c.records];
                        return acc;
                      }
                      return c;
                    }, {}),
                    sortRecords$
                  );
              }),
              toArray(),
              tap<ProcessInventoryRecord[]>(cs => {
                this.reportsData = cs;
                this.origReportsData = cs;
                this.triggerFilters = true;

                this.filterData = this.origReportsData.reduce((acc, o) => {
                  return [...acc, ...o.records.map(r => ({ ...r, user: o.name }))];
                }, []) as any;
              }),
            )
        )

      )
      .subscribe();

    this.router.events
      .pipe(
        filter(e => e instanceof NavigationEnd && !!this.toast),
        map(() => {
          this.toast.dismiss();
        }),
        take(1)
      )
      .subscribe();

    this.printService.onPrint
      .pipe(
        filter(url => url === '/reports/users-report'),
        tap(this.print),
        takeUntil(this.unSubscribe)
      )
      .subscribe();
  }

  valueChanges({ value }: { value: FilterPanelFormData }) {
    Object.keys(value)
      .forEach(key => {
        this.filterList({ data: value[key] }, key);
      });
  }

  print = () => {
    const doc = new jsPDF();
    const totalPagesExp = '{total_pages_count_string}';
    doc.autoTable({
      margin: { top: 15, right: 1, bottom: 5, left: 1 },
      head: [
        { user: 'User', location: 'Loc', name: 'Name', date: 'Date', quantity: 'Qty', truck: 'Truck', status: 'Status' },
      ],
      body: this.reportsData.reduce((acc, d: any) => {
        return [...acc, ...d.records.map(r => ({
          location: r.location,
          name: r.name,
          quantity: r.quantity,
          truck: r.truck,
          status: r.status,
          user: d.name,
          date: moment(r.date).format('MM/DD/YYYY')
        }))];
      }, []),
      didDrawPage: function (data) {
        // Header
        doc.setFontSize(20);
        doc.setTextColor(40);
        doc.setFontStyle('normal');
        doc.text('Users Report', data.settings.margin.left, 10);

        // Footer
        let str = 'Page ' + doc.internal.getNumberOfPages();
        // Total page number plugin only available in jspdf v1.0+
        if (typeof doc.putTotalPages === 'function') {
          str = str + ' of ' + totalPagesExp;
        }
        doc.setFontSize(7);

        // jsPDF 1.4+ uses getWidth, <1.4 uses .width
        const pageSize = doc.internal.pageSize;
        const pageHeight = pageSize.height ? pageSize.height : pageSize.getHeight();
        doc.text(str, data.settings.margin.left, pageHeight - 3);
      },

    });

    if (typeof doc.putTotalPages === 'function') {
      doc.putTotalPages(totalPagesExp);
    }
    doc.save('table.pdf');
  }

  /**
   * filter
   *
   * @param {Event} $event
   * @param {string} type
   * @memberof ReportComponent
   */
  public async filter($event: Event, type: string) {
    switch (type) {
      case 'user':
        this.filterByUser($event);
        break;
      case 'name':
        this.filterByName($event, 'name');
        break;
      case 'truck':
        this.filterByName($event, 'truck');
        break;
      case 'status':
        this.filterByName($event, 'status');
        break;
      case 'date':
        this.filterByName($event, 'date');
        break;
    }
  }

  public ngOnDestroy() {
    this.unSubscribe.next();
    this.unSubscribe.complete();
  }


  /*************************************
   *
   * Private Methods
   *
   *************************************/

  /**
   * filterByUser
   *
   * @private
   * @param {*} $event
   * @memberof ReportComponent
   */
  private async filterByUser($event: Event) {
    const users = this.origReportsData
      .map(r => ({ name: r.name, value: r.name }))
      .sort(this.sort.bind(this, 'name'));
    users.unshift({ name: 'All', value: 'all' });
    const result = await this.filterPopover($event, users);
    this.filterList(result, 'user');
  }

  /**
   * filterByName
   *
   * @private
   * @param {*} $event
   * @param {*} prop
   * @memberof ReportComponent
   */
  private async filterByName($event: Event, prop: string) {
    let inventoryItems = this.origReportsData.map(r => r.records)
      .reduce((acc, r) => [...acc, ...r], [])
      .map(r => ({ name: r[prop], value: r[prop] }))
      .filter(r => r.name !== undefined)
      .reduce((acc: { name: string, value: string }[], r: { name: string, value: string }) => {
        return acc.some(a => a.name === r.name) ? acc : acc.concat(r);
      }, [])
      .sort(this.sort.bind(this, 'name'));

    if (prop === 'date') {
      inventoryItems = this.convertDateToReadable(inventoryItems);
    }

    inventoryItems.unshift({ name: 'All', value: 'all' });
    const result = await this.filterPopover($event, inventoryItems);
    this.filterList(result, prop);
  }

  /**
   * convertDateToReadable
   *
   * @private
   * @param {*} items
   * @returns
   * @memberof ReportComponent
   */
  private convertDateToReadable(items: { name: string, value: string }[]) {
    return items.map(i => {
      return {
        ...i,
        name: this.datePipe.transform(i.name)
      };
    });
  }

  /**
   * filterList
   *
   * @private
   * @param {{ data: string }} result
   * @param {string} type
   * @returns
   * @memberof ReportComponent
   */
  private filterList(result: { data: string }, type: string) {
    if (result.data === (undefined || null)) {
      return;
    }
    if (this.cache.has(type)) {
      this.reportsData = this.getResetFilterForType(type);
      if (result.data === 'all') {
        return;
      }
    }
    switch (type) {
      case 'user':
      case 'name':
      case 'truck':
      case 'status':
      case 'startDate':
      case 'endDate':
        this.reportsData = this.getFilterList(result, type);
        this.cache.set(type, result.data);
        break;

      default:
        break;
    }

  }


  /**
   * getResetFilterForType
   *
   * @private
   * @param {string} type
   * @returns
   * @memberof ReportComponent
   */
  private getResetFilterForType(type: string) {
    this.cache.delete(type);
    let resetReportsData = this.origReportsData.slice(0);

    this.cache.forEach((v, k) => {
      resetReportsData = this.getFilterList({ data: v }, k, resetReportsData);
    });

    return resetReportsData;
  }

  /**
   * getFilterList
   *
   * @private
   * @param {{ data: string }} result
   * @param {*} prop
   * @returns
   * @memberof ReportComponent
   */
  private getFilterList(result: { data: string }, prop: string, searchData = this.reportsData) {

    if (prop === 'startDate' || prop === 'endDate') {

      const method = prop === 'startDate' ? 'isSameOrAfter' : 'isSameOrBefore';

      return searchData
        .filter(d => d.records.some(r => moment(r.date)[method](result.data)))
        .reduce((acc, d) => {
          d = { ...d, records: d.records.filter(r => moment(r.date)[method](result.data)) };
          return [...acc, d];
        }, []);
    }

    if (prop === 'user') {
      return searchData
        .filter(d => d.name.toUpperCase().includes(result.data.toUpperCase()));
    }

    return searchData
      .filter(d => d.records.some(r => r[prop].toUpperCase().includes(result.data.toUpperCase())))
      .reduce((acc, d) => {
        d = { ...d, records: d.records.filter(r => r[prop].toUpperCase().includes(result.data.toUpperCase())) };
        return [...acc, d];
      }, []);
  }

  /**
   * sort
   *
   * @private
   * @param {string} prop
   * @param {{}} a
   * @param {{}} b
   * @returns
   * @memberof ReportComponent
   */
  private sort(prop: string, a: {}, b: {}) {
    const nameA = typeof a[prop] === 'number' ? a[prop] : a[prop].toUpperCase();
    const nameB = typeof b[prop] === 'number' ? b[prop] : b[prop].toUpperCase();
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
    return 0;

  }

  /**
   * createRecord
   *
   * @private
   * @param {CheckoutItem} checkOutItem
   * @returns
   * @memberof ReportComponent
   */
  private createRecord(checkOutItem: CheckoutItem) {
    const user = this.userQuery.getEntity(checkOutItem.userId);
    if (user === undefined) {
      return null;
    }
    const records = checkOutItem.items === undefined ? [] : checkOutItem.items
      .reduce((acc, p) => {
        if (this.inventoryQuery.hasEntity(p.id) && p.type !== 'remove') {
          const item = this.inventoryQuery.getEntity(p.id);
          const isCheckedOut = p.type === 'checkout-pending';
          const date = checkOutItem.date;
          const record = {
            ...item,
            quantity: p.quantity,
            isCheckedOut,
            date,
            status: p.type,
            truck: p.truckName
          };
          acc.push(record);
        }
        return acc;
      }, []);
    return { name: user.fullName, id: checkOutItem.id, date: checkOutItem.date, records, allChecked: false, userId: user.id };
  }

  /**
   * Filter selector popover
   *
   * @private
   * @param {string[]} names
   * @param {Event} ev
   * @returns
   * @memberof ReportComponent
   */
  private async filterPopover(ev: Event, names: any[]) {
    const popover = await this.popoverController.create({
      component: PopoverFilterComponent,
      event: ev,
      componentProps: { names }
    });

    await popover.present();

    return await popover.onDidDismiss() as { data: string };
  }

}
