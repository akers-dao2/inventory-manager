import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModules } from '../shared/modules/shared.modules';
import { ReportsComponent } from './reports.component';
import { ReportComponent } from './report/report.component';
import { NavigationComponent } from './navigation/navigation.component';
import { InventoryReportComponent } from './inventory-report/inventory-report.component';
import { QuantityByDateComponent } from './quantity-by-date/quantity-by-date.component';
import { FilterPanelComponent } from './filter-panel/filter-panel.component';

@NgModule({
  imports: [
    SharedModules,
    RouterModule.forChild([
      {
        path: '',
        component: ReportsComponent,
        children: [
          {
            path: '',
            component: NavigationComponent,
          },
          {
            path: 'users-report',
            component: ReportComponent,
          },
          {
            path: 'inventory-report',
            component: InventoryReportComponent,
          },
          {
            path: 'quantity-by-date-report',
            component: QuantityByDateComponent,
          },
        ]
      }
    ])
  ],
  declarations: [
    ReportsComponent,
    ReportComponent,
    NavigationComponent,
    InventoryReportComponent,
    QuantityByDateComponent,
    FilterPanelComponent
  ],
  entryComponents: [
    ReportComponent,
    NavigationComponent
  ]
})
export class ReportsComponentModule { }
