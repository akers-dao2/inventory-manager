import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, OnDestroy, ViewChild } from '@angular/core';
import { Platform, PopoverController, ToastController, ModalController, ActionSheetController, AlertController, LoadingController, IonInfiniteScroll } from '@ionic/angular';
import { PopoverQuantitySelectionComponent } from '../shared/components/popover-quantity-selection/popover-quantity-selection.component';
import { PopoverMenuComponent } from '../shared/components/popover-menu/popover-menu.component';
import { InventoryItem } from '../shared/tables/state/inventory.model';
import { InventoryQuery, InventoryService, UsersQuery, CheckoutService, CheckoutQuery, User, ItemIdData, UsersService } from '../shared/tables/state';
import { tap, filter, switchAll, toArray, take, map, distinctUntilChanged, bufferCount, skip, switchMap, switchMapTo } from 'rxjs/operators';
import { filterNil, EntityStateHistoryPlugin, ID } from '@datorama/akita';
import { PopoverItemInfoComponent } from '../shared/components/popover-item-info/popover-item-info.component';
import { AddOrUpdateComponent } from '../add-or-update/add-or-update.component';
import { Subscription, combineLatest, Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';
import { AngularFireAuth } from '@angular/fire/auth';
import { LoginService } from '../shared/services/login.service';
import { TrucksQuery } from '../shared/tables/state/trucks.query';
import { TrucksService } from '../shared/tables/state/trucks.service';
import { ActionSheetButton } from '@ionic/core';
import { SortInventoryService } from '../shared/services/sort-inventory.service';


@Component({
  selector: 'app-inventory-list',
  templateUrl: './inventory-list.component.html',
  styleUrls: ['./inventory-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InventoryListComponent implements OnInit, OnDestroy {

  public view = 'inventory';
  public inventoryList: InventoryItem[] = [];
  public inventoryLists: InventoryItem[][] = [];
  public isTabletOrDesktop = this.plt.is('tablet') || this.plt.is('desktop');
  public isEditMode = false;
  public searchInput = '';
  public displayCheckOutBtn = false;
  public displayQueueSegmentBtn = false;
  public stateHistory: EntityStateHistoryPlugin<InventoryItem>;
  public activeUser: User;
  private subscriptions: Subscription[] = [];
  public users: User[];
  public isAdmin: boolean;
  private loader: HTMLIonLoadingElement;
  private inventoryListIndex = 0;
  private isSearching = false;
  public hasRepairItem = false;
  @ViewChild(IonInfiniteScroll) infiniteScroll: IonInfiniteScroll;

  constructor(
    public plt: Platform,
    private popoverController: PopoverController,
    private inventoryQuery: InventoryQuery,
    private inventoryService: InventoryService,
    private toastController: ToastController,
    private userQuery: UsersQuery,
    private userService: UsersService,
    private checkoutService: CheckoutService,
    private checkoutQuery: CheckoutQuery,
    public modalController: ModalController,
    public actionSheetController: ActionSheetController,
    private ref: ChangeDetectorRef,
    private alertController: AlertController,
    private afs: AngularFirestore,
    public afAuth: AngularFireAuth,
    public loginService: LoginService,
    public loadingController: LoadingController,
    private trucksQuery: TrucksQuery,
    private trucksService: TrucksService,
    private sortBy: SortInventoryService
  ) { }

  ngOnInit() {
    this.afAuth.authState
      .pipe(
        filterNil,
        take(1)
      )
      .subscribe(async () => {
        this.loader = await this.createLoader();
        this.loader.present();
        this.initialize();
      });

    this.subscription = this.loginService.isAdmin
      .pipe(
        tap(isAdmin => {
          this.isAdmin = isAdmin;
        })
      )
      .subscribe();

    // this.inventoryService.batchLoad();
  }

  /**
   * createLoader
   *
   * @returns
   * @memberof InventoryListComponent
   */
  public createLoader() {
    return this.loadingController.create({
      message: 'Loading...'
    });
  }

  /**
   * reorder
   *
   * @param {*} ev
   * @memberof InventoryListComponent
   */
  public reorder(ev) {
    this.inventoryList = this.reorderArray(this.inventoryList, ev.detail.from, ev.detail.to);
    this.inventoryService.updateOrder(this.inventoryList);
    ev.detail.complete();
  }

  /**
   * reorderArray
   *
   * @param {any[]} arr
   * @param {number} from
   * @param {number} to
   * @returns
   * @memberof InventoryListComponent
   */
  public reorderArray(arr: any[], from: number, to: number) {
    arr.splice(to, 0, arr.splice(from, 1)[0]);
    return arr;
  }

  /**
   * onSelectChange
   *
   * @param {*} { detail }
   * @memberof InventoryListComponent
   */
  public onSelectChange({ detail }) {
    this.afs.collection('users').doc<User>(detail.value)
      .valueChanges()
      .pipe(
        tap(user => {
          this.loader.present();
          this.userService.loadUser(user);
          this.checkoutService.setActiveForActiveUser();
          this.resetAllInventoryItems();
          this.inventoryService.loadCheckOutForUser();
        }),
        take(1)
      )
      .subscribe();
  }


  ngOnDestroy(): void {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

  /**
   * Handle segment change event
   *
   * @param {any} event
   * @memberof InventoryListComponent
   */
  public segmentChanged(view: string) {
    switch (view) {
      case 'inventory':
        this.inventoryListIndex = 0;
        this.infiniteScroll.disabled = false;
        this.loadInventory();
        break;

      case 'queue':
        this.inventoryListIndex = 0;
        this.infiniteScroll.disabled = false;
        this.loadInventoryQueue();
        break;
    }
  }

  /**
   * Handle Checkbox event
   *
   * @param {InventoryItem} item
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  public async onCheckboxChange(item: InventoryItem, ev: Event, device: string) {

    switch (true) {
      case !item.isChecked && this.view === 'inventory':
        this.updateQuantity(item, ev, 'pending');
        break;

      case !item.isChecked && this.view === 'queue':
        item = {
          ...item,
          quantity: undefined
        };
        break;

      default:
        device === 'desktop' ? this.removeCheckedOutItem(item) : this.presentActionSheet(item, ev);
        break;
    }
  }

  /**
   * On quantity label click
   *
   * @param {InventoryItem} item
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  public async onQuantityLabelClick(item: InventoryItem, ev: Event) {
    if (item.status !== 'checkout') {
      this.updateQuantity(item, ev, 'update');
    }
  }

  /**
   * Handle enabling edit mode
   *
   * @memberof InventoryListComponent
   */
  public async editItem(id: ID) {
    const entity = this.inventoryQuery.getEntity(id);
    const modal = await this.modalController.create({
      component: AddOrUpdateComponent,
      componentProps: { entity }
    });
    return await modal.present();

  }

  /**
   * Handle done event for edit mode
   *
   * @memberof InventoryListComponent
   */
  public onDone() {
    this.isEditMode = false;
    this.ref.markForCheck();
  }

  /**
   * onSearch
   *
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  public onSearch(ev: Event) {
    if (this.searchInput.toLowerCase() === '') {
      this.isSearching = false;
      this.infiniteScroll.disabled = false;
      this.inventoryListIndex = 0;
      this.loadInventory();
      return;
    }

    this.isSearching = true;
    this.inventoryQuery.selectAll()
      .pipe(
        take(1),
        switchAll(),
        filter(v => v.name.toLowerCase().includes(this.searchInput.toLowerCase())),
        toArray(),
        tap(v => this.inventoryList = v)
      )
      .subscribe();
  }

  /**
   * Display popover menu
   *
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  public async presentPopoverMenu(ev: Event, id: string) {
    const popover = await this.popoverController.create({
      component: PopoverMenuComponent,
      event: event,
      componentProps: {
        list: [
          { name: 'Edit', value: 'edit', icon: 'create', show: true },
          { name: 'Remove', value: 'remove', icon: 'trash', show: true },
          { name: 'Restock', value: 'restock', icon: 'color-fill', show: true }
        ]
      }
    });

    await popover.present();

    const v = await popover.onDidDismiss();

    if (v.data && v.data !== null) {
      switch (v.data.item) {
        case 'edit':
          this.editItem(id);
          break;
        case 'remove':
          const result = await this.confirmAction();
          if (result) {
            this.inventoryService.remove(id);
            this.checkoutService.remove(id);
          }
          break;
        case 'restock':
          const quantity = await this.quantitySelectorPopover('Restock', ev, 0, true);
          this.restockItem(id, quantity);
          break;
        case 'reorder':
          this.isEditMode = true;
          this.ref.markForCheck();
          break;
        default:
          break;
      }
    }
  }

  /**
   * Handle restocking of an iventory item
   *
   * @param {ID} id
   * @param {{ data: number; }} quantity
   * @returns {*}
   * @memberof InventoryListComponent
   */
  public restockItem(id: ID, quantity: { data: number; }): any {
    if (quantity.data) {
      const entity = this.inventoryQuery.getEntity(id);
      const available = parseInt(entity.available.toString(), 10) + parseInt(quantity.data.toString(), 10);
      this.inventoryService.upsert({ ...entity, available });
      this.inventoryService.setActive(entity.id);
    }
  }

  /**
   * On info click
   *
   * @param {InventoryItem} item
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  public async onInfoClick(item: InventoryItem, ev: Event) {
    const popover = await this.popoverController.create({
      component: PopoverItemInfoComponent,
      event: ev,
      componentProps: { item }
    });

    await popover.present();
  }

  public loadMore(event) {
    const numRows = this.inventoryLists.length - 1;
    if (numRows === this.inventoryListIndex || this.isSearching || this.view === 'queue') {
      event.target.disabled = true;
      return;
    }
    this.inventoryList = this.inventoryList.concat(this.inventoryLists[++this.inventoryListIndex]);
    event.target.complete();
  }

  public doRefresh(event) {
    event.target.complete();
  }

  public async showTruckSelection() {

    const selectedTruckId = this.trucksQuery.getActiveId();
    const radios = this.trucksQuery.getAllSorted()
      .map<any>(t => {
        return {
          name: t.name,
          type: 'radio',
          label: t.name,
          value: t.id,
          checked: selectedTruckId === t.id
        };
      });
    const alert = await this.alertController.create({
      header: 'Truck Selection',
      inputs: radios,
      buttons: [{
        text: 'Cancel',
        role: 'cancel',
      }, {
        text: 'Change',
        handler: id => this.trucksService.setActive(id)
      }]
    });

    await alert.present();

    return new Promise((resolve) => {
      alert.onDidDismiss().then(() => {
        resolve();
      });
    });

  }

  public async markForRepair(checkedOutInventoryItemID: string) {
    const checkoutItems = this.checkoutQuery
      .getAll({ filterBy: c => c.userId === this.activeUser.id && c.items.some(i => !['checkin', 'remove'].includes(i.type) && i.id === checkedOutInventoryItemID) });
    const item = checkoutItems[0].items.find(i => i.id === checkedOutInventoryItemID);
    this.checkoutService.createOrReplace({ ...item, type: 'broken' }, this.activeUser);
    await this.checkoutService.updateAllCheckOutType(checkoutItems[0].id);

    this.inventoryService.markBroken(checkedOutInventoryItemID);
  }


  /*************************************
   *
   * Private Methods
   *
   *************************************/

  /**
   * initialize
   *
   * @private
   * @memberof InventoryListComponent
   */
  private async initialize() {
    this.subscription = combineLatest(
      this.userQuery.selectActive(),
      this.afs.collection<User>('users').valueChanges()
    )
      .pipe(
        tap(([user, users]) => {
          this.activeUser = user;
          this.users = users;
          this.ref.markForCheck();
        }),
        switchMapTo(this.checkoutQuery
          .selectAll({ filterBy: c => c.userId === this.activeUser.id && c.items.some(i => ['pending', 'checkout', 'partial'].includes(i.type)) })),
        tap(r => this.displayQueueSegmentBtn = !!r.length)
      )
      .subscribe();

    this.loadInventory(true);



    this.subscription = (this.inventoryQuery.selectActive() as Observable<InventoryItem>)
      .pipe(
        skip(1),
        filterNil,
        distinctUntilChanged(),
        tap(v => {
          const index = this.inventoryList.findIndex(i => i.id === v.id);
          const hasItem = this.inventoryLists.some(l => l.some(i => i.id === v.id));
          if (index === -1 && !hasItem) {
            if (this.inventoryList.length <= 50) {
              this.inventoryList.push(v);
            }
          } else if (index !== -1) {
            this.inventoryList[index] = { ...this.inventoryList[index], ...v };
            this.refreshInventoryList();
            if (this.view === 'queue') {
              this.loadInventoryQueue();
            }
          }
          this.ref.markForCheck();
        })
      )
      .subscribe();

    this.subscription = this.checkoutQuery.selectAll({ filterBy: c => c.items === undefined ? false : c.items.some(i => i.isDirty) })
      .pipe(
        switchAll(),
        filter(() => this.checkoutQuery.getActive() !== undefined),
        map(v => v.items.filter(p => p.isDirty)),
        filter(v => !!v.length),
        tap(async ids => {
          const onlyIds = ids.map(i => i.id);
          this.inventoryService.updateAvails(ids);
          this.checkoutService.markPristene(onlyIds);
        })
      )
      .subscribe();

    this.subscription = this.inventoryQuery.selectError()
      .pipe(
        filterNil,
        tap(({ msg, item }) => {
          this.presentToast(msg);
          this.resetInventoryItem(item, { isChecked: false });
          this.inventoryService.resetErrorState();
        })
      )
      .subscribe();

    this.subscription = this.inventoryService.selectRemoveItem()
      .pipe(
        tap(id => {
          this.inventoryList = this.inventoryList.filter(i => i.id !== id);
          this.ref.markForCheck();
        })
      )
      .subscribe();
  }

  private loadInventory(initial = false) {
    const filterByInventoryView = filter<InventoryItem[]>(() => this.view === 'inventory');
    const filterEmptyArr = filter<InventoryItem[]>(v => !!v.length);
    const filterBySearch = filter<InventoryItem>(r => {
      return this.searchInput === '' ? true : r.name.toLowerCase().includes(this.searchInput.toLowerCase());
    });

    this.inventoryQuery.selectAll()
      .pipe(
        filterByInventoryView,
        filterEmptyArr,
        this.sortBy.name,
        take(1),
        switchAll(),
        filterBySearch,
        bufferCount(50),
        toArray(),
        tap(v => {
          if (initial) {
            this.inventoryLists = v;
          }
          this.inventoryList = v[this.inventoryListIndex];
          this.loader.dismiss();
          this.ref.markForCheck();
        })
      )
      .subscribe();
  }

  refreshInventoryList() {
    this.inventoryQuery.selectAll()
      .pipe(
        this.sortBy.name,
        take(1),
        switchAll(),
        bufferCount(50),
        toArray(),
        tap<InventoryItem[][]>(v => this.inventoryLists = v)
      )
      .subscribe();
  }

  private loadInventoryQueue() {
    const inventoryItems = this.inventoryQuery.getAll({ filterBy: v => v.isChecked && !v.broken });
    this.inventoryList = inventoryItems;
    if (!inventoryItems.length) {
      this.view = 'inventory';
      this.segmentChanged('inventory');
    }

    this.hasRepairItem = inventoryItems.some(i => i.allowRepairs);
  }

  /**
   * Quantity selector popover
   *
   * @private
   * @param {string} [title='Quantity']
   * @param {Event} ev
   * @returns
   * @memberof InventoryListComponent
   */
  private async quantitySelectorPopover(title: string = 'Quantity', ev: Event, quantity = 0, backdropDismiss = false) {
    const popover = await this.popoverController.create({
      component: PopoverQuantitySelectionComponent,
      event: ev,
      componentProps: { title, quantity }
    });

    await popover.present();

    return await popover.onDidDismiss() as { data: number };
  }

  /**
   * Present toast
   *
   * @private
   * @param {*} message
   * @memberof InventoryListComponent
   */
  private async presentToast(message: string) {
    const toast = await this.toastController.create({
      message,
      duration: 2000
    });
    toast.present();
  }

  /**
   * Update quantity for selected item
   *
   * @private
   * @param {InventoryItem} inventoryItem
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  private async updateQuantity(inventoryItem: InventoryItem, ev: Event, type: 'pending' | 'update') {
    const [checkoutItems, checkoutItemsByDate, checkoutList] = await Promise.all([
      this.checkoutQuery.getAllNotCheckInItems$(),
      this.checkoutQuery.getAllNotCheckInItems$(false),
      this.checkoutQuery.getOpenCheckOutList()
    ]);
    let isAvailable = false;

    let existingQuantity: number;
    // extract existingQuantity
    let checkoutItemByDate: ItemIdData;
    if (checkoutItems) {
      checkoutItemByDate = checkoutItemsByDate.find(v => v.id === inventoryItem.id);

      switch (true) {
        case checkoutItemByDate !== undefined:
          existingQuantity = checkoutItemByDate.quantity;
          break;

        default:
          existingQuantity = 0;
          break;
      }
    }
    // get quantity
    const quantity = (await this.quantitySelectorPopover('Quantity', ev, existingQuantity)).data;
    if (quantity === undefined) {
      if (checkoutItemByDate === undefined && (!inventoryItem.isChecked || inventoryItem.quantity === 0)) {
        this.resetInventoryItem(inventoryItem, { isChecked: false });
      }
      return;
    }

    if (!inventoryItem.allowZeroQty) {
      isAvailable = this.inventoryService.updateAvailAndQuantity(inventoryItem, quantity, existingQuantity || 0);
    }

    if (isAvailable || inventoryItem.allowZeroQty) {
      if (checkoutItemByDate && checkoutItemByDate.checkOutId) {
        this.checkoutService.setActive(checkoutItemByDate.checkOutId);
      } else if (checkoutList) {
        // add to items when only checkoutitems match date
        this.checkoutService.setActive(checkoutList.id);
      }

      if (this.checkoutQuery.getActive() === undefined) {
        await this.showTruckSelection();
      }

      setTimeout(() => {
        this.checkoutService.createOrReplace(
          {
            id: inventoryItem.id,
            quantity: quantity,
            previousQuantity: existingQuantity,
            isDirty: true,
            type
          },
          this.activeUser
        );
      }, 1000);
    }
  }

  /**
   * Present action sheet
   *
   * @private
   * @param {InventoryItem} item
   * @param {Event} ev
   * @memberof InventoryListComponent
   */
  private async presentActionSheet(inventoryItem: InventoryItem, ev: Event) {
    const buttons = [{
      text: 'Remove',
      role: 'destructive',
      icon: 'trash',
      handler: () => {
        this.removeCheckedOutItem(inventoryItem);
      }
    },
    {
      text: 'Update Quantity',
      icon: 'share',
      handler: () => {
        this.updateQuantity(inventoryItem, ev, 'update');
      }
    },
    {
      text: 'Repair',
      icon: 'build',
      handler: () => {
        this.markForRepair(inventoryItem.id);
      }
    },
    {
      text: 'Cancel',
      role: 'cancel',
      handler: () => {
        this.resetInventoryItem(inventoryItem, { isChecked: true });
      }
    }]
      .filter(b => inventoryItem.allowRepairs || b.text !== 'Repair')
      .filter(b => this.filterActionButtons(inventoryItem, b));

    const actionSheet = await this.actionSheetController.create({
      header: 'Options',
      buttons
    });
    await actionSheet.present();
  }

  filterActionButtons(inventoryItem: InventoryItem, button: ActionSheetButton) {
    if ((inventoryItem.status === 'checkout' || inventoryItem.broken) && ['Remove', 'Update Quantity'].includes(button.text)) {
      return false;
    }

    return true;
  }

  /**
   * Remove checked-out item from queue
   *
   * @private
   * @param {InventoryItem} inventoryItem
   * @memberof InventoryListComponent
   */
  private async removeCheckedOutItem(inventoryItem: InventoryItem) {
    const checkoutItems = await this.checkoutQuery.getAllNotCheckInItems$();
    const checkoutItem = checkoutItems.find(v => v.id === inventoryItem.id && !['checkin', 'partial'].includes(v.type));

    if (checkoutItem) {
      this.checkoutService.setActive(checkoutItem.checkOutId);
      setTimeout(() => {
        this.checkoutService.createOrReplace({
          id: inventoryItem.id,
          type: 'remove',
          isDirty: true,
          quantity: checkoutItem.quantity,
          previousQuantity: 0
        }, this.activeUser);
      }, 1000);
    } else {
      this.resetInventoryItem(inventoryItem, { isChecked: true });
      this.presentToast('Error: This item cannot be unchecked, it is checked out.');
    }
  }

  /**
   * Track observable subscriptions
   *
   * @private
   * @param {Subscription} subscription
   * @memberof InventoryListComponent
   */
  private set subscription(subscription: Subscription) {
    this.subscriptions.push(subscription);
  }

  /**
   * Confirm Action
   *
   * @private
   * @returns {Promise<boolean>}
   * @memberof InventoryListComponent
   */
  private confirmAction(): Promise<boolean> {
    return new Promise(async (resolve) => {
      const alert = await this.alertController.create({
        header: 'Confirm',
        message: 'Remove Item',
        buttons: [
          {
            text: 'No',
            role: 'cancel',
            handler: () => resolve(false)
          }, {
            text: 'Yes',
            handler: () => resolve(true)
          }
        ]

      });

      await alert.present();
    });
  }

  private resetInventoryItem(item: InventoryItem, props: Partial<InventoryItem>) {
    const index = this.inventoryList.findIndex(i => i.id === item.id);
    this.inventoryList[index] = { ...item, ...props };
    this.ref.markForCheck();
  }

  private resetAllInventoryItems() {
    this.inventoryList
      .filter(i => i.isChecked || i.isCheckedIn || i.isCheckedOut)
      .forEach(i => {
        const index = this.inventoryList.findIndex(v => v.id === i.id);
        this.inventoryList[index] = {
          ...this.inventoryList[index],
          isChecked: false,
          quantity: 0,
          status: 'untouched',
          isCheckedOut: false,
          isCheckedIn: false
        };
      });

    this.inventoryService.resetAllCheckStates();
  }
}
