import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { InventoryListComponent } from './inventory-list.component';
import { SharedModules } from '../shared/modules/shared.modules';

@NgModule({
  imports: [
    SharedModules,
    RouterModule.forChild([
      {
        path: '',
        component: InventoryListComponent
      }
    ])
  ],
  declarations: [
    InventoryListComponent
  ]
})
export class InventoryListComponentModule { }
