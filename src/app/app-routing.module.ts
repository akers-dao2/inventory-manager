import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'inventory',
    pathMatch: 'full'
  },
  {
    path: 'add',
    loadChildren: './add-or-update/add-or-update.module#AddOrUpdateComponentModule'
  },
  {
    path: 'inventory',
    loadChildren: './inventory-list/inventory-list.module#InventoryListComponentModule'
  },
  {
    path: 'process-inventory',
    loadChildren: './process-inventory-request/process-inventory-request.module#ProcessInventoryRequestModule'
  },
  {
    path: 'list-users',
    loadChildren: './users/list-users/list-users.module#ListUsersModule'
  },
  {
    path: 'reports',
    loadChildren: './reports/reports.module#ReportsComponentModule'
  },
  {
    path: 'trucks',
    loadChildren: './trucks/trucks.module#TrucksModule'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
